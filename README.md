# VWS-Spielwiese

This is a demonstration for active and proactive AAS using the BaSyx framework.  
Please [see the wiki](https://gitlab.hrz.tu-chemnitz.de/vws-demo/vws-spielwiese/-/wikis/home) for more information.
