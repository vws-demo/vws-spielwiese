#!/bin/bash

# (c) 2022 Oliver Parczyk
# This code is licensed under MIT license (see LICENSE.txt for details)


SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export PORT=4000
export REGISTRYPATH="http://main.local:4000/registry"
export AASSERVERPATH="http://main.local:4001/aasServer"

wait_for_registry () {
  until curl $REGISTRYPATH/api/v1/registry 2>/dev/null 1>&2
  do
    echo "waiting for registry to come up..."
    sleep 2s
  done
}

wait_for_aas_server () {
  until curl $AASSERVERPATH/shells 2>/dev/null 1>&2
  do
    echo "waiting for AAS server to come up..."
    sleep 2s
  done
}

wait_for_mqtt_broker () {
  until mosquitto_pub -h $MQTTBROKERHOST -t "test" -m "test" 2>/dev/null 1>&2
  do
    echo "waiting for MQTT broker to come up..."
    sleep 2s
  done
}

if [ $HOSTNAME == "registry" ]
then
  $SCRIPT_DIR/start_component.sh registry
elif [ $HOSTNAME == "aas-server" ]
then
  wait_for_registry
  $SCRIPT_DIR/start_component.sh server
elif [ $HOSTNAME == "light-controller" ]
then
  wait_for_registry
  wait_for_aas_server
  $SCRIPT_DIR/start_component.sh controller
elif [ $HOSTNAME == "light-animator" ]
then
  wait_for_registry
  wait_for_aas_server
  $SCRIPT_DIR/start_component.sh animator
elif [ $HOSTNAME == "light-0" ]
then
  wait_for_registry
  wait_for_aas_server
  export RED_LIGHTS=2
  export RED_PINS=[3,17]
  export GREEN_LIGHTS=2
  export GREEN_PINS=[2,4]
  $SCRIPT_DIR/start_component.sh lights
elif [ $HOSTNAME == "light-1" ]
then
  wait_for_registry
  wait_for_aas_server
  export RED_LIGHTS=2
  export RED_PINS=[3,17]
  export GREEN_LIGHTS=2
  export GREEN_PINS=[2,4]
  $SCRIPT_DIR/start_component.sh lights
elif [ $HOSTNAME == "light-2" ]
then
  wait_for_registry
  wait_for_aas_server
  export RED_LIGHTS=2
  export RED_PINS=[22,17]
  export GREEN_LIGHTS=2
  export GREEN_PINS=[23,27]
  $SCRIPT_DIR/start_component.sh lights
  elif [ $HOSTNAME == "light-3" ]
then
  wait_for_registry
  wait_for_aas_server
  export RED_LIGHTS=2
  export RED_PINS=[22,17]
  export GREEN_LIGHTS=2
  export GREEN_PINS=[23,27]
  $SCRIPT_DIR/start_component.sh lights
elif [ $HOSTNAME == "sensor-strand" ]
then
  wait_for_registry
  wait_for_aas_server
  export TEMPERATURE_SENSORS=1
  export ANALOG_TEMPERATURE_SENSOR_CHANNEL=0
  export DIGITAL_TEMPERATURE_SENSOR_PIN=22
  export SOUND_SENSORS=1
  export DIGITAL_SOUND_SENSOR_PIN=23
  export ANALOG_SOUND_SENSOR_CHANNEL=1
  export LIGHT_SENSORS=1
  export DIGITAL_LIGHT_SENSOR_PIN=27
  export JOYSTICK_SENSORS=1
  export ANALOG_JOYSTICK_SENSOR_CHANNEL_X=2
  export ANALOG_JOYSTICK_SENSOR_CHANNEL_Y=3
  export DIGITAL_TEMPERATURE_HUMID_SENSORS=1
  export DIGITAL_TEMPERATURE_HUMID_SENSOR_PIN=22
  $SCRIPT_DIR/start_component.sh sensors
elif [ $HOSTNAME == "esp-controller" ]
then
  wait_for_registry
  wait_for_aas_server
  sudo docker run --network host -v $SCRIPT_DIR/mosquitto.conf:/mosquitto/config/mosquitto.conf eclipse-mosquitto 2>/dev/null 1>&2 &
  until mosquitto_pub -h esp-controller.local -t "test" -m "test" 2>/dev/null 1>&2
  do
    echo "waiting for MQTT broker to come up..."
    sleep 2s
  done
  export RED_LIGHTS=3
  export GREEN_LIGHTS=3
  $SCRIPT_DIR/start_component.sh proprietary_lights
elif [ $HOSTNAME == "aasx-server" ]
then
  sudo docker run --network host adminshellio/aasx-server-blazor-for-demo-arm32

elif [ $HOSTNAME == "rfid" ]
then
  wait_for_registry
  wait_for_aas_server
  wait_for_mqtt_broker
  export MQTTBROKERHOST=esp-controller.local
  export READERID=0
  export READERAMOUNT=1
  $SCRIPT_DIR/start_component.sh rfid

elif [ $HOSTNAME == "lcd_monitor" ]
then
  wait_for_registry
  wait_for_aas_server
  wait_for_mqtt_broker
  export MQTTBROKERHOST=esp-controller.local
  export DISPLAYID=0
  export DISPLAYAMOUNT=1
  $SCRIPT_DIR/start_component.sh lcd_monitor

else
  echo "Your hostname '$HOSTNAME' is not recognized as part of the demonstration."
  echo "This script is intended to start up the demonstration in normal operation."
  echo "please use start_component.sh and setup your environment for manual operation."
fi
