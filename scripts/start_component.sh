#!/bin/bash

# (c) 2022 Oliver Parczyk
# This code is licensed under MIT license (see LICENSE.txt for details)


export CLASSPATH=$HOME/.m2/repository/org/eclipse/basyx/basyx.components.lib/1.0.2/basyx.components.lib-1.0.2.jar:$HOME/.m2/repository/org/postgresql/postgresql/42.2.2/postgresql-42.2.2.jar:$HOME/.m2/repository/org/camunda/bpm/camunda-engine/7.15.0/camunda-engine-7.15.0.jar:$HOME/.m2/repository/org/camunda/bpm/model/camunda-cmmn-model/7.15.0/camunda-cmmn-model-7.15.0.jar:$HOME/.m2/repository/org/camunda/bpm/dmn/camunda-engine-dmn/7.15.0/camunda-engine-dmn-7.15.0.jar:$HOME/.m2/repository/org/camunda/commons/camunda-commons-utils/1.10.0/camunda-commons-utils-1.10.0.jar:$HOME/.m2/repository/org/camunda/bpm/model/camunda-dmn-model/7.15.0/camunda-dmn-model-7.15.0.jar:$HOME/.m2/repository/org/camunda/bpm/dmn/camunda-engine-feel-api/7.15.0/camunda-engine-feel-api-7.15.0.jar:$HOME/.m2/repository/org/camunda/bpm/dmn/camunda-engine-feel-juel/7.15.0/camunda-engine-feel-juel-7.15.0.jar:$HOME/.m2/repository/org/camunda/bpm/dmn/camunda-engine-feel-scala/7.15.0/camunda-engine-feel-scala-7.15.0.jar:$HOME/.m2/repository/org/camunda/feel/feel-engine/1.13.1/feel-engine-1.13.1-scala-shaded.jar:$HOME/.m2/repository/org/camunda/commons/camunda-commons-logging/1.10.0/camunda-commons-logging-1.10.0.jar:$HOME/.m2/repository/org/camunda/commons/camunda-commons-typed-values/7.15.0/camunda-commons-typed-values-7.15.0.jar:$HOME/.m2/repository/org/mybatis/mybatis/3.5.6/mybatis-3.5.6.jar:$HOME/.m2/repository/org/springframework/spring-beans/5.2.8.RELEASE/spring-beans-5.2.8.RELEASE.jar:$HOME/.m2/repository/joda-time/joda-time/2.1/joda-time-2.1.jar:$HOME/.m2/repository/org/camunda/connect/camunda-connect-core/1.5.2/camunda-connect-core-1.5.2.jar:$HOME/.m2/repository/org/camunda/connect/camunda-connect-connectors-all/1.5.2/camunda-connect-connectors-all-1.5.2.jar:$HOME/.m2/repository/org/camunda/bpm/model/camunda-bpmn-model/7.15.0/camunda-bpmn-model-7.15.0.jar:$HOME/.m2/repository/org/camunda/bpm/model/camunda-xml-model/7.15.0/camunda-xml-model-7.15.0.jar:$HOME/.m2/repository/commons-io/commons-io/2.6/commons-io-2.6.jar:$HOME/.m2/repository/com/h2database/h2/1.4.197/h2-1.4.197.jar:$HOME/.m2/repository/com/zaxxer/HikariCP/3.4.5/HikariCP-3.4.5.jar:$HOME/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar:$HOME/.m2/repository/org/eclipse/basyx/basyx.sdk/1.0.2/basyx.sdk-1.0.2.jar:$HOME/.m2/repository/org/eclipse/milo/sdk-client/0.3.6/sdk-client-0.3.6.jar:$HOME/.m2/repository/org/eclipse/milo/stack-client/0.3.6/stack-client-0.3.6.jar:$HOME/.m2/repository/com/digitalpetri/netty/netty-channel-fsm/0.3/netty-channel-fsm-0.3.jar:$HOME/.m2/repository/com/digitalpetri/fsm/strict-machine/0.1/strict-machine-0.1.jar:$HOME/.m2/repository/io/netty/netty-codec-http/4.1.34.Final/netty-codec-http-4.1.34.Final.jar:$HOME/.m2/repository/io/netty/netty-common/4.1.34.Final/netty-common-4.1.34.Final.jar:$HOME/.m2/repository/io/netty/netty-buffer/4.1.34.Final/netty-buffer-4.1.34.Final.jar:$HOME/.m2/repository/io/netty/netty-transport/4.1.34.Final/netty-transport-4.1.34.Final.jar:$HOME/.m2/repository/io/netty/netty-resolver/4.1.34.Final/netty-resolver-4.1.34.Final.jar:$HOME/.m2/repository/org/eclipse/milo/sdk-core/0.3.6/sdk-core-0.3.6.jar:$HOME/.m2/repository/org/eclipse/milo/bsd-parser-core/0.3.6/bsd-parser-core-0.3.6.jar:$HOME/.m2/repository/com/sun/activation/jakarta.activation/1.2.1/jakarta.activation-1.2.1.jar:$HOME/.m2/repository/org/glassfish/jaxb/jaxb-runtime/2.3.2/jaxb-runtime-2.3.2.jar:$HOME/.m2/repository/jakarta/xml/bind/jakarta.xml.bind-api/2.3.2/jakarta.xml.bind-api-2.3.2.jar:$HOME/.m2/repository/org/glassfish/jaxb/txw2/2.3.2/txw2-2.3.2.jar:$HOME/.m2/repository/com/sun/istack/istack-commons-runtime/3.0.8/istack-commons-runtime-3.0.8.jar:$HOME/.m2/repository/org/jvnet/staxex/stax-ex/1.8.1/stax-ex-1.8.1.jar:$HOME/.m2/repository/com/sun/xml/fastinfoset/FastInfoset/1.2.16/FastInfoset-1.2.16.jar:$HOME/.m2/repository/jakarta/activation/jakarta.activation-api/1.2.1/jakarta.activation-api-1.2.1.jar:$HOME/.m2/repository/org/eclipse/milo/sdk-server/0.3.6/sdk-server-0.3.6.jar:$HOME/.m2/repository/org/eclipse/milo/stack-core/0.3.6/stack-core-0.3.6.jar:$HOME/.m2/repository/org/bouncycastle/bcprov-jdk15on/1.61/bcprov-jdk15on-1.61.jar:$HOME/.m2/repository/org/bouncycastle/bcpkix-jdk15on/1.61/bcpkix-jdk15on-1.61.jar:$HOME/.m2/repository/com/google/guava/guava/26.0-jre/guava-26.0-jre.jar:$HOME/.m2/repository/io/netty/netty-codec/4.1.34.Final/netty-codec-4.1.34.Final.jar:$HOME/.m2/repository/io/netty/netty-handler/4.1.34.Final/netty-handler-4.1.34.Final.jar:$HOME/.m2/repository/org/eclipse/milo/stack-server/0.3.6/stack-server-0.3.6.jar:$HOME/.m2/repository/javax/servlet/javax.servlet-api/4.0.1/javax.servlet-api-4.0.1.jar:$HOME/.m2/repository/javax/ws/rs/javax.ws.rs-api/2.1.1/javax.ws.rs-api-2.1.1.jar:$HOME/.m2/repository/org/glassfish/jersey/core/jersey-client/2.30/jersey-client-2.30.jar:$HOME/.m2/repository/jakarta/ws/rs/jakarta.ws.rs-api/2.1.6/jakarta.ws.rs-api-2.1.6.jar:$HOME/.m2/repository/org/glassfish/jersey/core/jersey-common/2.30/jersey-common-2.30.jar:$HOME/.m2/repository/jakarta/annotation/jakarta.annotation-api/1.3.5/jakarta.annotation-api-1.3.5.jar:$HOME/.m2/repository/org/glassfish/hk2/osgi-resource-locator/1.0.3/osgi-resource-locator-1.0.3.jar:$HOME/.m2/repository/org/glassfish/hk2/external/jakarta.inject/2.6.1/jakarta.inject-2.6.1.jar:$HOME/.m2/repository/org/glassfish/jersey/inject/jersey-hk2/2.30.1/jersey-hk2-2.30.1.jar:$HOME/.m2/repository/org/glassfish/hk2/hk2-locator/2.6.1/hk2-locator-2.6.1.jar:$HOME/.m2/repository/org/glassfish/hk2/external/aopalliance-repackaged/2.6.1/aopalliance-repackaged-2.6.1.jar:$HOME/.m2/repository/org/glassfish/hk2/hk2-api/2.6.1/hk2-api-2.6.1.jar:$HOME/.m2/repository/org/glassfish/hk2/hk2-utils/2.6.1/hk2-utils-2.6.1.jar:$HOME/.m2/repository/org/javassist/javassist/3.25.0-GA/javassist-3.25.0-GA.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-catalina/8.5.41/tomcat-catalina-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-servlet-api/8.5.41/tomcat-servlet-api-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-jsp-api/8.5.41/tomcat-jsp-api-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-el-api/8.5.41/tomcat-el-api-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-juli/8.5.41/tomcat-juli-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-annotations-api/8.5.41/tomcat-annotations-api-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-api/8.5.41/tomcat-api-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-jni/8.5.41/tomcat-jni-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-coyote/8.5.41/tomcat-coyote-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-util/8.5.41/tomcat-util-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-util-scan/8.5.41/tomcat-util-scan-8.5.41.jar:$HOME/.m2/repository/org/apache/tomcat/tomcat-jaspic-api/8.5.41/tomcat-jaspic-api-8.5.41.jar:$HOME/.m2/repository/ch/qos/logback/logback-classic/1.2.3/logback-classic-1.2.3.jar:$HOME/.m2/repository/ch/qos/logback/logback-core/1.2.3/logback-core-1.2.3.jar:$HOME/.m2/repository/org/codehaus/janino/janino/2.7.5/janino-2.7.5.jar:$HOME/.m2/repository/org/codehaus/janino/commons-compiler/2.7.5/commons-compiler-2.7.5.jar:$HOME/.m2/repository/com/google/code/gson/gson/2.8.5/gson-2.8.5.jar:$HOME/.m2/repository/org/eclipse/basyx/basyx.components.registry/1.0.4/basyx.components.registry-1.0.4.jar:$HOME/.m2/repository/org/mongodb/mongodb-driver-sync/4.0.5/mongodb-driver-sync-4.0.5.jar:$HOME/.m2/repository/org/mongodb/bson/4.0.5/bson-4.0.5.jar:$HOME/.m2/repository/org/mongodb/mongodb-driver-core/4.0.5/mongodb-driver-core-4.0.5.jar:$HOME/.m2/repository/org/springframework/data/spring-data-mongodb/3.0.2.RELEASE/spring-data-mongodb-3.0.2.RELEASE.jar:$HOME/.m2/repository/org/springframework/spring-tx/5.2.8.RELEASE/spring-tx-5.2.8.RELEASE.jar:$HOME/.m2/repository/org/springframework/spring-context/5.2.8.RELEASE/spring-context-5.2.8.RELEASE.jar:$HOME/.m2/repository/org/springframework/spring-aop/5.2.8.RELEASE/spring-aop-5.2.8.RELEASE.jar:$HOME/.m2/repository/org/springframework/spring-core/5.2.8.RELEASE/spring-core-5.2.8.RELEASE.jar:$HOME/.m2/repository/org/springframework/spring-jcl/5.2.8.RELEASE/spring-jcl-5.2.8.RELEASE.jar:$HOME/.m2/repository/org/springframework/spring-expression/5.2.8.RELEASE/spring-expression-5.2.8.RELEASE.jar:$HOME/.m2/repository/org/springframework/data/spring-data-commons/2.3.2.RELEASE/spring-data-commons-2.3.2.RELEASE.jar:$HOME/.m2/repository/org/eclipse/basyx/basyx.components.AASServer/1.0.2/basyx.components.AASServer-1.0.2.jar:$HOME/.m2/repository/org/apache/poi/poi-ooxml/4.1.2/poi-ooxml-4.1.2.jar:$HOME/.m2/repository/org/apache/poi/poi/4.1.2/poi-4.1.2.jar:$HOME/.m2/repository/commons-codec/commons-codec/1.13/commons-codec-1.13.jar:$HOME/.m2/repository/org/apache/commons/commons-collections4/4.4/commons-collections4-4.4.jar:$HOME/.m2/repository/org/apache/commons/commons-math3/3.6.1/commons-math3-3.6.1.jar:$HOME/.m2/repository/com/zaxxer/SparseBitSet/1.2/SparseBitSet-1.2.jar:$HOME/.m2/repository/org/apache/poi/poi-ooxml-schemas/4.1.2/poi-ooxml-schemas-4.1.2.jar:$HOME/.m2/repository/org/apache/xmlbeans/xmlbeans/3.1.0/xmlbeans-3.1.0.jar:$HOME/.m2/repository/org/apache/commons/commons-compress/1.19/commons-compress-1.19.jar:$HOME/.m2/repository/com/github/virtuald/curvesapi/1.06/curvesapi-1.06.jar:$HOME/.m2/repository/org/eclipse/paho/org.eclipse.paho.client.mqttv3/1.2.0/org.eclipse.paho.client.mqttv3-1.2.0.jar:$HOME/.m2/repository/junit/junit/4.12/junit-4.12.jar:$HOME/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar:$HOME/.m2/repository/com/fasterxml/jackson/core/jackson-databind/2.9.8/jackson-databind-2.9.8.jar:$HOME/.m2/repository/com/fasterxml/jackson/core/jackson-annotations/2.9.0/jackson-annotations-2.9.0.jar:$HOME/.m2/repository/com/fasterxml/jackson/core/jackson-core/2.9.8/jackson-core-2.9.8.jar
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export CLASSPATH=$CLASSPATH:$SCRIPT_DIR/../basyx.lichterkette/target/classes

if [ -z ${1+x} ]
then
  echo "You need to specify which part to start: 'start_component.sh [component]'"
  echo "Valid components are: registry, server, lights, controller, animator, proprietary_lights, rfid, lcd_monitor"
  exit 1
fi

if [ $1 == "registry" ]
then
  java de.olipar.basyx.lichterkette.RegistryServer

elif [ $1 == "server" ]
then
  java de.olipar.basyx.lichterkette.AASServer

elif [ $1 == "lights" ]
then
  java de.olipar.basyx.lichterkette.Lightstrand

elif [ $1 == "sensors" ]
then
  java de.olipar.basyx.lichterkette.Sensorstrand

elif [ $1 == "controller" ]
then
  java de.olipar.basyx.lichterkette.LightControllerSubmodelProvider

elif [ $1 == "animator" ]
then
  java de.olipar.basyx.lichterkette.LightAnimatorSubmodelProvider

elif [ $1 == "proprietary_lights" ]
then
  java de.olipar.basyx.lichterkette.ProprietaryLightstrand

elif [ $1 == "rfid" ]
then
  cd /home/pi/vws-spielwiese/basyx.lichterkette/resources/sensors
  python3 mqtt_handler_rfid.py &
  java de.olipar.basyx.lichterkette.RfidController

elif [ $1 == "lcd_monitor" ]
then
  cd /home/pi/vws-spielwiese/basyx.lichterkette/resources/sensors
  python3 mqtt_handler_lcd.py &
  java de.olipar.basyx.lichterkette.LcdDisplayController

else
  echo "$1 is not a valid component."
  echo "Valid components are: registry, server, lights, controller, animator, proprietary_lights, rfid, lcd_monitor"
  exit 1
fi
