# (c) 2022 Daniel Kluge
# This code is licensed under MIT license (see ../LICENSE.txt for details)

import time
from threading import Thread

import board
import busio
import digitalio
import httpx

import adafruit_pcd8544

# Here we set all the pns for the LCD
spi = busio.SPI(board.SCK, MOSI=board.MOSI)
dc = digitalio.DigitalInOut(board.D24)
cs = digitalio.DigitalInOut(board.D8)
reset = digitalio.DigitalInOut(board.D23)
display = adafruit_pcd8544.PCD8544(spi, dc, cs, reset)
# Settings for contrast and bias
display.bias = 4
display.contrast = 60

# Settings for the monitoring
timer_per_sensor = 5
aas_server = "http://main.local:4001"

sensor_values = {}

def background_fetch(sensor_list):
    global sensor_values

    fetch_list = []
    for sensor in sensor_list:
        for method in sensor[1]:
            fetch_list.append((sensor[0], method))
    next_fetch_idx = 0

    with httpx.Client() as client:
        while True:
            next_fetch = fetch_list[next_fetch_idx]
            sensor_name = next_fetch[0]
            method = next_fetch[1]
            try:
                # Fetch sensor values
                resp = client.post(f"{aas_server}/aasServer/shells/urn:de.olipar.basyx:sensor-strand.local:{sensor_name}/aas/submodels/sensor/submodel/submodelElements/read{method}Value/invoke", timeout=5)
                response = resp.json()

                success = response.get("success", True)
                if not success:
                    time.sleep(0.1)
                    continue

                # Update values
                sensor_class = list(response.keys())[0]
                values = list(response.values())[0]
                sensor_values[sensor_name]["Model"] = values["model"]

                if sensor_class == "lightSensor":
                    sensor_values[sensor_name]["Bright"] = str(values["isBright"])
                    sensor_values[sensor_name]["Dark"] = str(values["isDark"])
                elif sensor_class == "tempSensor":
                    if method == "Digital":
                        sensor_values[sensor_name][">D_THR"] = str(values["threshold_reached"])
                    else:
                        voltage = values["raw_value"] * 5 / 1024

                        sensor_values[sensor_name][">A_THR"] = str(values["hand_detected"])
                        sensor_values[sensor_name]["U"] = f"{voltage:.2f}V"
                        sensor_values[sensor_name]["Gain"] = f"{values['gain']:.2f}"
                elif sensor_class == "soundSensor":
                    if method == "Digital":
                        sensor_values[sensor_name][">D_THR"] = str(values["sound_detected"])
                    else:
                        voltage = values["raw_value"] * 5 / 1024

                        sensor_values[sensor_name][">A_THR"] = str(values["loud_sound_detected"])
                        sensor_values[sensor_name]["U"] = f"{voltage:.2f}V"
                        sensor_values[sensor_name]["Gain"] = f'{values["gain"]:.2f}'
                elif sensor_class == "joystickSensor":
                    # Postions for stick in [-1, 1]
                    x_pos = (values["values"]["x_value"]["raw_value"] / 2048) - 1 
                    y_pos = (values["values"]["x_value"]["raw_value"] / 2048) - 1 

                    # Defaul pos strings are too long (eg. "up right")
                    pos = str(values["joystick_state"]).replace("up", "^").replace("down", "v").replace("left", "<").replace("right", ">")

                    sensor_values[sensor_name]["X"] = f'{x_pos:.3}'
                    sensor_values[sensor_name]["Y"] = f'{y_pos:.3}'
                    sensor_values[sensor_name]["Pos"] = pos

                # Normally the DHT would be here but it's broken

            except Exception as e:
                # print(e)
                pass

            next_fetch_idx = (next_fetch_idx + 1) % len(fetch_list)
            time.sleep(0.1)

def display_show(sensors):
    selected_sensor = 0
    count = len(sensors)
    while True:
        switch_time = time.time() + timer_per_sensor
        while time.time() < switch_time:
            display.fill(0)

            # Local variable so we minimize race conditions
            values = sensor_values[sensors[selected_sensor]].copy()

            # Display the sensor name
            display.text(values["Sensor"], 0, 0, 1)

            for line, (key, value) in enumerate(values.items()):
                if key == "Sensor": continue
                # Sensor is always the first so no more line + 1
                elif key == "Model": display.text(f"{value}", 0, (line) * 8, 1)
                else: display.text(f"{key}: {value}", 0, (line) * 8, 1)

            #display.text(f"hello {selected_sensor}", 0, 0, 1)
            #display.text("this is the", 0, 8, 1)
            #display.text("CircuitPython", 0, 16, 1)
            #display.text("adafruit lib-", 0, 24, 1)
            #display.text("rary for the", 0, 32, 1)
            #display.text("PCD8544! :) ", 0, 40, 1)
            display.show()
            time.sleep(0.1)
        # We have 5 sensors on our sensorstrand but DHT is broken
        selected_sensor = (selected_sensor + 1) % count
        

if __name__ == "__main__":
    # Create the background threads that fetch current values
    to_fetch = [
        ("TemperatureSensor_0", ["Analog", "Digital"]),
        ("SoundSensor_0", ["Analog", "Digital"]),
        ("LightSensor_0", ["Digital"]),
        ("JoystickSensor_0", ["Analog"]),
        #("DigitalTemperatureSensor_0", ["Digital"])
        ]
    
    for sensor in to_fetch:
        # Here we populate the sensor_values dict
        sensor_values[sensor[0]] = {"Sensor": sensor[0]}
    
    t = Thread(target=background_fetch, args=[to_fetch], daemon=True)
    t.start()
    
    display_show(list(map(lambda x: x[0], to_fetch)))
