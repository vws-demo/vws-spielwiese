# To build on your own machine you probably want to use "maven:3-jdk-11-slim".
# Add "--build-arg MVN_IMAGE=maven:3-jdk-11-slim" to your docker build command.
ARG MVN_IMAGE=arm32v7/maven:3-eclipse-temurin-11
# The jre image should work on amd64 and arm32v7, so no need to change.
# In case you still _want_ to change the image, use "--build-arg JRE_IMAGE=..." to the build command.
ARG JRE_IMAGE=eclipse-temurin:11-jre

# Because only our sensors need python etc, we only have to install the necessary
# dependencies when necessary
ARG IS_SENSOR

FROM $MVN_IMAGE as build
WORKDIR /root/app
COPY pom.xml .
RUN mvn dependency:resolve
RUN mvn dependency:build-classpath -Dmdep.outputFile=classpath.txt
COPY src src
RUN mvn compile

FROM $JRE_IMAGE
WORKDIR /root/app
# We need mosquitto_pub and sub for the proprietary lights. It's only half an MiB.
# We need python for the sensor reading scripts
RUN apt-get update && if [[ -z "$IS_SENSOR" ]]; then apt-get install -y mosquitto-clients; else apt-get install -y mosquitto-clients libgpiod2 python3 python3-pip build-essential python3-dev gpiod; fi && rm -rf /var/lib/apt/lists/*
COPY --from=build /root/.m2 /root/.m2
COPY --from=build /root/app/target/classes classes
COPY --from=build /root/app/classpath.txt .

COPY ./resources/requirements.txt requirements.txt
RUN if [[ -z "$IS_SENSOR" ]]; then python3 -m pip install -r requirements.txt; fi && rm requirements.txt

COPY ./resources/sensors ./sensor-scripts
COPY ./resources/start_component_docker.sh start_component.sh

ENTRYPOINT export CLASSPATH=/root/app/classes:$(cat /root/app/classpath.txt) && /root/app/start_component.sh