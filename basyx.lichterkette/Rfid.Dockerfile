# To build on your own machine you probably want to use "maven:3-jdk-11-slim".
# Add "--build-arg MVN_IMAGE=maven:3-jdk-11-slim" to your docker build command.
ARG MVN_IMAGE=arm32v7/maven:3-eclipse-temurin-11
# The jre image should work on amd64 and arm32v7, so no need to change.
# In case you still _want_ to change the image, use "--build-arg JRE_IMAGE=..." to the build>
ARG JRE_IMAGE=eclipse-temurin:11-jre

FROM $MVN_IMAGE as build
WORKDIR /root/app
COPY pom.xml .
RUN mvn dependency:resolve
RUN mvn dependency:build-classpath -Dmdep.outputFile=classpath.txt
COPY src src
RUN mvn compile

FROM $JRE_IMAGE
WORKDIR /root/app

ENV IN_DOCKER=true

RUN apt-get update && apt-get install -y mosquitto-clients libgpiod2 python3 python3-pip build-essential python3-dev gpiod && rm -rf /var/lib/apt/lists/*
COPY --from=build /root/.m2 /root/.m2
COPY --from=build /root/app/target/classes classes
COPY --from=build /root/app/classpath.txt .

COPY ./resources/requirements_rfid.txt requirements.txt
RUN pip3 install -r requirements.txt && rm requirements.txt

COPY ./resources/sensors ./sensor-scripts
COPY ./resources/start_component_docker.sh start_component.sh

ENTRYPOINT export CLASSPATH=/root/app/classes:$(cat /root/app/classpath.txt) && /root/app/start_component.sh

