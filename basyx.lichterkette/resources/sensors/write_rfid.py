import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
import sys
from datetime import datetime
import json
import paho.mqtt.client as mqtt
from os import environ

brokerAddress = environ.get('MQTTBROKERHOST')
readerId = environ.get('READERID')

chipId = 0
chipText = ""
timestamp = None

def createDict():
    client.connect(brokerAddress)
    # create dictionary containing the data and convert it to string
    rfidDict = {'reader_id': 'RfidReader_' + str(readerId),
                'model': 'AZ-Delivery RFID RC522 Reader',
                'chip_id': chipId,
                'chip_text': chipText.strip(),
                'timestamp': timestamp
                }
    jsonDict = {'rfidReader': rfidDict}
    jsonString = json.dumps(rfidDict, indent=4, default=str)
    print(jsonString)
    client.publish("rfidReader/RfidReader_"+str(readerId),jsonString)


client = mqtt.Client("RfidReader_0")
reader = SimpleMFRC522()

# try to write a chip once, afterwards it is read and the result published via MQTT
try:
    text = sys.argv[1]
    print("Place the tag on the reader.")
    reader.write(text)
    print("Finished writing.")
    chipId, chipText = reader.read()
    timestamp = datetime.now()
    createDict()
finally:
    GPIO.cleanup()