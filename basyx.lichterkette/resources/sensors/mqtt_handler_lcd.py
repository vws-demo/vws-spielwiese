import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
from os import environ
from time import sleep
import subprocess
import sys

brokerAddress = environ.get('MQTTBROKERHOST')
displayId = environ.get('DISPLAYID')

topicCycle = "display/Lcd_"+displayId+"/cycle"
topicText = "display/Lcd_"+displayId+"/text"

proc = None

# subscribe to topics when connecting to broker
def on_connect(client, userdata, flags, rc):
    client.subscribe(topicCycle)
    client.subscribe(topicText)

# start scripts when message is received
def on_message(client, userdata, message):
    # try to decode payload
    payload = None
    try:
        payload = str(message.payload.decode("utf-8"))
        print("message received:", payload)
        print("message topic:", message.topic)
    except Exception as e:
        print("Error. Could not decode payload: ")
        print(e)

    global proc

    # kill proc if some script is already running
    try:
        proc.kill()
    except Exception as e:
        print(e)

    # start python scripts depending on topic of message
    if(message.topic == topicCycle):
        try:
            proc = subprocess.Popen([sys.executable, 'cycle_lcd.py'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except Exception as e:
            print("Error. Could not start cycle: ")
            print(e)

    if(message.topic == topicText):
        try:
            text = payload
            proc = subprocess.Popen([sys.executable, 'display_text_lcd.py', text], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except Exception as e:
            print("Error. Could not display text: ")
            print(e)

client = mqtt.Client("display")
client.on_connect = on_connect
client.on_message = on_message
client.connect(brokerAddress)
client.loop_forever()
