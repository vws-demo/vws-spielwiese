#!/usr/bin/python
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
from time import sleep
from datetime import datetime
import json
import paho.mqtt.client as mqtt
from os import environ

brokerAddress = environ.get('MQTTBROKERHOST')
readerId = environ.get('READERID')

chipId = 0
chipText = ""
timestamp = None

reader = SimpleMFRC522()
client = mqtt.Client("rfid-rc522")

def createDict():
    client.connect(brokerAddress)
    # create dictionary containing the data and convert it to string
    rfidDict = {'reader_id': 'RfidReader_' + str(readerId),
                'model': 'AZ-Delivery RFID RC522 Reader',
                'chip_id': chipId,
                'chip_text': chipText.strip(),
                'timestamp': timestamp
                }
    jsonDict = {'rfidReader': rfidDict}
    jsonString = json.dumps(rfidDict, indent=4, default=str)
    print(jsonString)
    client.publish("rfidReader/RfidReader_"+str(readerId), jsonString)
    client.disconnect()

# every time a chip is detected the MQTT message is published
try:
    while(True):
        try:
            print("Put a chip on the reader")
            chipId, chipText = reader.read()
            timestamp = datetime.now()
            createDict()
            # delay to avoid that tags are read again
            sleep(3)
        finally:
            chip_detected = False
finally:        
    GPIO.cleanup()