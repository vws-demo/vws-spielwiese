from os import environ
import RPi.GPIO as GPIO
import time
import json

sensor_pin = int(environ.get('DIGITAL_SOUND_SENSOR_PIN', 27))
GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor_pin, GPIO.IN)
sound_detected = False
counter = 0


def callback(channel):
    sound_sensor_dict = {'model': 'Joy-It KY-038 Soundsensor',
                         'type': 'digital',
                         'sensor_pin': sensor_pin,
                         'sound_detected': True
                         }
    json_dict = {'soundSensor': sound_sensor_dict}

    jsonString = json.dumps(json_dict, indent=4)
    print(jsonString)
    globals()['sound_detected'] = True


GPIO.add_event_detect(sensor_pin, GPIO.BOTH, bouncetime=300)
GPIO.add_event_callback(sensor_pin, callback)

while True:
    if sound_detected:
        break
    time.sleep(1)
    counter = counter + 1
    if counter == 3:
        sound_sensor_dict = {'model': 'Joy-It KY-038 Soundsensor',
                             'type': 'digital',
                             'sensor_pin': sensor_pin,
                             'sound_detected': False
                             }
        json_dict = {'soundSensor': sound_sensor_dict}
        jsonString = json.dumps(json_dict, indent=4)
        print(jsonString)
        break
