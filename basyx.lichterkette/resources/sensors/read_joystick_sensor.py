from os import environ
from MCP3008 import MCP3008
import json

read_channel_x = int(environ.get('ANALOG_JOYSTICK_SENSOR_CHANNEL_X', 2))
read_channel_y = int(environ.get('ANALOG_JOYSTICK_SENSOR_CHANNEL_Y', 3))

adc = MCP3008()


def detect_joystick_state(vrx_raw, vry_raw):
    if (2000 < vrx_raw < 2100) and (1950 < vry_raw < 2050):
        return "middle"
    if (4000 < vrx_raw < 4100) and (1950 < vry_raw < 2050):
        return "up"
    if (-50 < vrx_raw < 50) and (1950 < vry_raw < 2050):
        return "down"
    if (2000 < vrx_raw < 2100) and (-50 < vry_raw < 50):
        return "left"
    if (2000 < vrx_raw < 2100) and (4000 < vry_raw < 4100):
        return "right"
    if (4000 < vrx_raw < 4100) and (-50 < vry_raw < 50):
        return "up left"
    if (4000 < vrx_raw < 4100) and (4000 < vry_raw < 4100):
        return "up right"
    if (-50 < vrx_raw < 50) and (-50 < vry_raw < 50):
        return "down left"
    if (-50 < vrx_raw < 50) and (4000 < vry_raw < 4100):
        return "down right"
    return "unknown"


# get raw values from joystick sensor
joystick_sensor_vrx_raw = adc.read(channel=read_channel_x)
joystick_sensor_vry_raw = adc.read(channel=read_channel_y)

joystick_value_dict = {'x_value': {'mcp3008_channel': read_channel_x,
                                   'raw_value': joystick_sensor_vrx_raw
                                   },
                       'y_value': {'mcp3008_channel': read_channel_y,
                                   'raw_value': joystick_sensor_vry_raw
                                   }
                       }
joystick_sensor_dict = {'model': 'Joy-It KY-023 Joystick',
                        'type': 'analog',
                        'values': joystick_value_dict,
                        'joystick_state': detect_joystick_state(joystick_sensor_vrx_raw, joystick_sensor_vry_raw)
                        }
json_dict = {'joystickSensor': joystick_sensor_dict}
jsonString = json.dumps(json_dict, indent=4)
print(jsonString)
