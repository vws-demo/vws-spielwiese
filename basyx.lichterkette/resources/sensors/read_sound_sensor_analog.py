from os import environ
from MCP3008 import MCP3008
import json

read_channel = int(environ.get('ANALOG_SOUND_SENSOR_CHANNEL', 1))

adc = MCP3008()


def detect_sound_state(gain):
    if gain < 10:
        return True
    else:
        return False


# get raw value from sound sensor
sound_sensor_value = adc.read(channel=read_channel)
sound_sensor_gain = sound_sensor_value * (5.0 / 1023.0)

sound_sensor_dict = {'model': 'Joy-It KY-038 Soundsensor',
                     'type': 'analog',
                     'mcp3008_channel': read_channel,
                     'raw_value': sound_sensor_value,
                     'gain': sound_sensor_gain,
                     'loud_sound_detected': detect_sound_state(sound_sensor_gain)
                     }
json_dict = {'soundSensor': sound_sensor_dict}
jsonString = json.dumps(json_dict, indent=4)
print(jsonString)
