import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
from os import environ
from time import sleep
import subprocess
import sys

brokerAddress = environ.get('MQTTBROKERHOST')
readerId = environ.get('READERID')

topicLoop = "rfidReader/RfidReader_"+readerId+"/loop"   # to start the loop, reading tags infinitely
topicRead = "rfidReader/RfidReader_"+readerId+"/read"   # read one tag
topicWrite = "rfidReader/RfidReader_"+readerId+"/write" # change text of one tag
topicStop = "rfidReader/RfidReader_"+readerId+"/stop"   # stop proc

proc = None

# subscribe to topics when connecting to broker
def on_connect(client, userdata, flags, rc):
    client.subscribe(topicLoop)
    client.subscribe(topicRead)
    client.subscribe(topicWrite)
    client.subscribe(topicStop)

# start scripts when message is received
def on_message(client, userdata, message):
    # try to decode payload
    payload = None
    try:
        payload = str(message.payload.decode("utf-8"))
        print("message received:", payload)
        print("message topic:", message.topic)
    except Exception as e:
        print("Error. Could not decode payload: ")
        print(e)

    global proc

    # kill proc if some script is already running
    try:
        proc.kill()
    except Exception as e:
        print(e)

    # start python scripts depending on topic of message
    if(message.topic == topicLoop):
        try:
            proc = subprocess.Popen([sys.executable, 'read_rfid_loop.py'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except Exception as e:
            print("Error. Could not start loop: ")
            print(e)

    if(message.topic == topicRead):
        try:
            proc = subprocess.Popen([sys.executable, 'read_rfid.py'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except Exception as e:
            print("Error. Could not read tag: ")
            print(e)
    
    if(message.topic == topicWrite):
        try:
            text = payload
            proc = subprocess.Popen([sys.executable, 'write_rfid.py', text], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        except Exception as e:
            print("Error. Could not write tag: ")
            print(e)

client = mqtt.Client("rfidReader")
client.on_connect = on_connect
client.on_message = on_message
client.connect(brokerAddress)
client.loop_forever()
