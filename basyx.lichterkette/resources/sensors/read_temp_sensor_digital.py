from os import environ
import RPi.GPIO as GPIO
import json

GPIO.setmode(GPIO.BCM)
sensor_pin = int(environ.get('DIGITAL_TEMPERATURE_SENSOR_PIN', 22))
GPIO.setup(sensor_pin, GPIO.IN)
temp_sensor_raw_value = GPIO.input(sensor_pin)
threshold_reached = False
if temp_sensor_raw_value:
    threshold_reached = True
else:
    threshold_reached = False
temp_sensor_dict = {'model': 'Joy-It KY-028 Thermistor',
                    'type': 'digital',
                    'sensor_pin': sensor_pin,
                    'threshold_reached': threshold_reached
                    }
json_dict = {'tempSensor': temp_sensor_dict}

jsonString = json.dumps(json_dict, indent=4)
print(jsonString)
