from os import environ
import RPi.GPIO as GPIO
import json

GPIO.setmode(GPIO.BCM)
sensor_pin = int(environ.get('DIGITAL_LIGHT_SENSOR_PIN', 23))
GPIO.setup(sensor_pin, GPIO.IN)
light_sensor_raw_value = GPIO.input(sensor_pin)
isBright = False
isDark = False
if light_sensor_raw_value:
    isDark = True
else:
    isBright = True

light_sensor_dict = {'model': 'Joy-It KY-018 Fotowiderstand',
                     'type': 'digital',
                     'sensor_pin': sensor_pin,
                     'isBright': isBright,
                     'isDark': isDark,
                     }
json_dict = {'lightSensor': light_sensor_dict}

jsonString = json.dumps(json_dict, indent=4)
print(jsonString)
