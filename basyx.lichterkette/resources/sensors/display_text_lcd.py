import sys

import board
import busio
import digitalio

import adafruit_pcd8544

# Here we set all the pns for the LCD
spi = busio.SPI(board.SCK, MOSI=board.MOSI)
dc = digitalio.DigitalInOut(board.D24)
cs = digitalio.DigitalInOut(board.D8)
reset = digitalio.DigitalInOut(board.D23)
display = adafruit_pcd8544.PCD8544(spi, dc, cs, reset)
# Settings for contrast and bias
display.bias = 4
display.contrast = 60

try:
    # argument should conain lines comma-separated
    lines = sys.argv[1].split(',')

    for idx, line in enumerate(lines):
        display.text(f"{line}", 0, (idx)*8, 1)

    display.show()

except Exception as e:
    print(e)