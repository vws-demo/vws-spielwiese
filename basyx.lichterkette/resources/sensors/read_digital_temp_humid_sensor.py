import sys
from os import environ, devnull
import time
import json
import board
import adafruit_dht

original_stdout = sys.stdout

# Disable printing to stdout
def no_print():
    global original_stdout
    original_stdout = sys.stdout
    sys.stdout = open(devnull, 'w')

# Enable it again
def enable_print():
    sys.stdout.close()
    sys.stdout = original_stdout

no_print()

read_pin = environ.get('DIGITAL_TEMPERATURE_HUMID_SENSOR_PIN', 17)
read_pin = getattr(board, f"D{read_pin}")

# Initial the dht device, with data pin connected to:
# dhtDevice = adafruit_dht.DHT22(board.D4)

# you can pass DHT22 use_pulseio=False if you wouldn't like to use pulseio.
# This may be necessary on a Linux single board computer like the Raspberry Pi,
# but it will not work in CircuitPython.
dhtDevice = adafruit_dht.DHT11(read_pin, use_pulseio=True)

timeout = time.time() + 3
success = False

while time.time() < timeout:
    try:
        # Print the values to the serial port
        temperature_c = dhtDevice.temperature
        if temperature_c is None: 
            time.sleep(0.1)
            continue
        temperature_f = temperature_c * (9 / 5) + 32
        humidity = dhtDevice.humidity
        temp_sensor_value_dict = {'celsius': '{:.1f} C'.format(temperature_c),
                                  'fahrenheit': '{:.1f} F'.format(temperature_f)
                                  }
        temp_sensor_dict = {'model': 'Joy-It KY-015 Kombi Sensor',
                            'type': 'digital',
                            'sensor_pin': read_pin,
                            'temperature_values': temp_sensor_value_dict,
                            'humidity': '{}%'.format(humidity)
                            }
        json_dict = {'tempSensor': temp_sensor_dict}

        jsonString = json.dumps(json_dict, indent=4)
        enable_print()
        print(jsonString)
        success = True
        break
    
    except Exception as error:
        time.sleep(0.1)
        continue

if not success:
    enable_print()
    print(json.dumps({"error": True, "message": "DHT not reachable"}))

no_print()
dhtDevice.exit()
