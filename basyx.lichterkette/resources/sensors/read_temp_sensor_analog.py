from os import environ
from MCP3008 import MCP3008
import json

read_channel = int(environ.get('ANALOG_TEMPERATURE_SENSOR_CHANNEL', 0))

adc = MCP3008()


def detect_temp_state(gain):
    if gain < 10:
        return True
    else:
        return False


# get raw value from temperature sensor
temp_sensor_value = adc.read(channel=read_channel)
temp_sensor_gain = temp_sensor_value * (5.0 / 1023.0)

temp_sensor_dict = {'model': 'Joy-It KY-028 Thermistor',
                    'type': 'analog',
                    'mcp3008_channel': read_channel,
                    'raw_value': temp_sensor_value,
                    'gain': temp_sensor_gain,
                    'hand_detected': detect_temp_state(temp_sensor_gain)
                    }
json_dict = {'tempSensor': temp_sensor_dict}

jsonString = json.dumps(json_dict, indent=4)
print(jsonString)
