
#!/usr/bin/python
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
from datetime import datetime
import json
import paho.mqtt.client as mqtt
from os import environ

brokerAddress = environ.get('MQTTBROKERHOST')
readerId = environ.get('READERID')

chipId = 0
chipText = ""
timestamp = None

reader = SimpleMFRC522()
client = mqtt.Client("rfid-rc522")
client.connect(brokerAddress)

def createDict():
    # create dictionary containing the data and convert it to string
    rfidDict = {'reader_id': 'RfidReader_' + str(readerId),
                 'model': 'AZ-Delivery RFID RC522 Reader',
                 'chip_id': chipId,
                 'chip_text': chipText.strip(),
                 'timestamp': timestamp
                }
    jsonDict = {'rfidReader': rfidDict}
    jsonString = json.dumps(rfidDict, indent=4, default=str)
    print(jsonString)
    client.publish("rfidReader/RfidReader_"+str(readerId),jsonString)

# try to read exactly one chip, result is published via MQTT
try:
    print("Put a chip on the reader")
    chipId, chipText = reader.read()
    timestamp = datetime.now()
    createDict()
finally:
    GPIO.cleanup()
