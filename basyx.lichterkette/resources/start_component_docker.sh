#!/bin/bash

# (c) 2022 Oliver Parczyk, Daniel Kluge
# This code is licensed under MIT license (see LICENSE.txt for details)

if [ -z ${COMPONENT+x} ]
then
  echo "You need to specify which part to start by setting the COMPONENT environment variable."
  echo "Valid components are: registry, server, lights, light_controller, light_animator, proprietary_lights, rfid, lcd_monitor"
  exit 1
fi

wait_for_registry () {
  until curl $REGISTRYPATH/api/v1/registry 2>/dev/null 1>&2
  do
    echo "waiting for registry to come up..."
    sleep 2s
  done
}

wait_for_aas_server () {
  until curl $AASSERVERPATH/shells 2>/dev/null 1>&2
  do
    echo "waiting for AAS server to come up..."
    sleep 2s
  done
}

wait_for_mqtt_broker () {
  until mosquitto_pub -h $MQTTBROKERHOST -t "test" -m "test" 2>/dev/null 1>&2
  do
    echo "waiting for MQTT broker to come up..."
    sleep 2s
  done
}

if [ $COMPONENT == "registry" ]
then
  java de.olipar.basyx.lichterkette.RegistryServer

elif [ $COMPONENT == "server" ]
then
  wait_for_registry
  java de.olipar.basyx.lichterkette.AASServer

elif [ $COMPONENT == "lights" ]
then
  wait_for_registry
  wait_for_aas_server
  java de.olipar.basyx.lichterkette.Lightstrand

elif [ $COMPONENT == "sensors" ]
then
  wait_for_registry
  wait_for_aas_server
  java de.olipar.basyx.lichterkette.Sensorstrand

elif [ $COMPONENT == "light_controller" ]
then
  wait_for_registry
  wait_for_aas_server
  java de.olipar.basyx.lichterkette.LightControllerSubmodelProvider

elif [ $COMPONENT == "light_animator" ]
then
  wait_for_registry
  wait_for_aas_server
  java de.olipar.basyx.lichterkette.LightAnimatorSubmodelProvider

elif [ $COMPONENT == "proprietary_lights" ]
then
  wait_for_registry
  wait_for_aas_server
  wait_for_mqtt_broker
  java de.olipar.basyx.lichterkette.ProprietaryLightstrand

elif [ $COMPONENT == "rfid" ]
then
  wait_for_registry
  wait_for_aas_server
  wait_for_mqtt_broker
  cd /root/app/sensor-scripts/
  python3 mqtt_handler_rfid.py &
  java de.olipar.basyx.lichterkette.RfidController

elif [ $COMPONENT == "lcd_monitor" ]
then
  wait_for_registry
  wait_for_aas_server
  wait_for_mqtt_broker
  cd /root/app/sensor-scripts/
  python3 mqtt_handler_lcd.py &
  java de.olipar.basyx.lichterkette.LcdDisplayController

else
  echo "$COMPONENT is not a valid component."
  echo "Valid components are: registry, server, lights, light_controller, light_animator, proprietary_lights, rfid, lcd_monitor"
  exit 1
fi
