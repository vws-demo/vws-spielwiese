/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk & Marcus Rothhaupt
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.slf4j.LoggerFactory;

/**
 * Sensorstrand. Creates 1 tempSensor, uses the
 * hostUploadAndRegister method from SensorSubmodelProvider to create,
 * register and upload the AAS as well as host and register the SensorSwitch
 * Submodules
 *
 * @author olipar, speedstuff
 */

public class Sensorstrand {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Sensorstrand.class);
    private static int tempSensorAmount;
    private static int lightSensorAmount;
    private static int soundSensorAmount;
    private static int joystickSensorAmount;
    private static int digitalTempHumidSensorAmount;

    private static int getSensorAmount(String sensor) {
        sensor = sensor.toUpperCase();
        String uncheckedAmount = System.getenv(sensor + "_SENSORS");
        if (uncheckedAmount == null || uncheckedAmount.isEmpty()) {
            logger.warn("No valid amount for " + sensor.toLowerCase() + " sensor set.");
            logger.warn("You might want to explicitly set a " + sensor + "_SENSORS environment variable, eg:");
            logger.warn("$ export " + sensor + "_SENSORS=4");
            logger.warn("No " + sensor.toLowerCase() + " sensors will be created.");
            return 0;
        }
        try {
            int uncheckedIntegerAmount = Integer.parseUnsignedInt(uncheckedAmount);
            if (uncheckedIntegerAmount < 0) {
                throw new IllegalArgumentException(sensor + "_SENSORS is negative");
            }
            return uncheckedIntegerAmount;
        } catch (IllegalArgumentException e) {
            logger.warn("No valid amount for " + sensor.toLowerCase() + " sensors set.");
            logger.warn("Make sure " + sensor.toLowerCase() + " is a positive or zero integer value, eg:");
            logger.warn("$ export " + sensor + "_SENSORS=4");
            logger.warn("No " + sensor.toLowerCase() + " sensors will be created.");
            return 0;
        }
    }

    public static boolean inDocker() {
        // Check if this process is run in Docker as we have different paths for the sensor scripts
        // https://stackoverflow.com/a/52581380
        try (Stream <String> stream = Files.lines(Paths.get("/proc/1/cgroup"))) {
            return stream.anyMatch(line -> line.contains("/docker"));
        } catch (IOException e) {
            return false;
        }
    }

    public static void main(String[] args) {
        boolean inDocker = inDocker();

        tempSensorAmount = getSensorAmount("temperature");
        soundSensorAmount = getSensorAmount("sound");
        lightSensorAmount = getSensorAmount("light");
        joystickSensorAmount = getSensorAmount("joystick");
        digitalTempHumidSensorAmount = getSensorAmount("digital_temperature_humid");

        final int HOSTPORT = Common.getHostPort();
        logger.info("Local host port is set to " + HOSTPORT);
        logger.info("Ports " + HOSTPORT + " through including " + (HOSTPORT + tempSensorAmount + soundSensorAmount + lightSensorAmount + joystickSensorAmount + digitalTempHumidSensorAmount - 1)
                + " will be used for sensors.");

        for (int i = 0; i < tempSensorAmount; i++) {
            TemperatureSensor sensor;
            sensor = new TemperatureSensor(Integer.toString(i), inDocker);
            SensorSubmodelProvider ssp = new SensorSubmodelProvider(
                    "Sensorstrand:TemperatureSensor" + sensor.getID());
            ssp.hostUploadAndRegister(sensor, "TemperatureSensor_" + sensor.getID(), "TemperatureSensor" + sensor.getID(),
                    HOSTPORT + i);
        }
        for (int i = 0; i < soundSensorAmount; i++) {
            SoundSensor sensor;
            sensor = new SoundSensor(Integer.toString(i), inDocker);
            SensorSubmodelProvider ssp = new SensorSubmodelProvider(
                    "Sensorstrand:SoundSensor_" + sensor.getID());
            ssp.hostUploadAndRegister(sensor, "SoundSensor_" + sensor.getID(), "SoundSensor_" + sensor.getID(),
                    HOSTPORT + tempSensorAmount + i);
        }
        for (int i = 0; i < lightSensorAmount; i++) {
            LightSensor sensor;
            sensor = new LightSensor(Integer.toString(i), inDocker);
            SensorSubmodelProvider ssp = new SensorSubmodelProvider(
                    "Sensorstrand:lightSensor_" + sensor.getID());
            ssp.hostUploadAndRegister(sensor, "LightSensor_" + sensor.getID(), "lightSensor_" + sensor.getID(),
                    HOSTPORT + tempSensorAmount + soundSensorAmount + i);
        }
        for (int i = 0; i < joystickSensorAmount; i++) {
            JoystickSensor sensor;
            sensor = new JoystickSensor(Integer.toString(i), inDocker);
            SensorSubmodelProvider ssp = new SensorSubmodelProvider(
                    "Sensorstrand:JoystickSensor_" + sensor.getID());
            ssp.hostUploadAndRegister(sensor, "JoystickSensor_" + sensor.getID(), "JoystickSensor_" + sensor.getID(),
                    HOSTPORT + tempSensorAmount + soundSensorAmount + lightSensorAmount + i);
        }
        for (int i = 0; i < digitalTempHumidSensorAmount; i++) {
            DigitalTemperatureHumiditySensor sensor;
            sensor = new DigitalTemperatureHumiditySensor(Integer.toString(i), inDocker);
            SensorSubmodelProvider ssp = new SensorSubmodelProvider(
                    "Sensorstrand:DigitalTemperatureSensor_" + sensor.getID());
            ssp.hostUploadAndRegister(sensor, "DigitalTemperatureSensor_" + sensor.getID(), "DigitalTemperatureSensor_" + sensor.getID(),
                    HOSTPORT + tempSensorAmount + soundSensorAmount + lightSensorAmount + joystickSensorAmount + i);
        }
    }

}

