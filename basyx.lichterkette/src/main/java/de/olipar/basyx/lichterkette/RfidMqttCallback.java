/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.basyx.aas.manager.ConnectedAssetAdministrationShellManager;
import org.eclipse.basyx.aas.metamodel.connected.ConnectedAssetAdministrationShell;
import org.eclipse.basyx.aas.metamodel.map.descriptor.ModelUrn;
import org.eclipse.basyx.aas.registration.proxy.AASRegistryProxy;
import org.eclipse.basyx.submodel.metamodel.api.ISubmodel;
import org.eclipse.basyx.submodel.metamodel.api.submodelelement.operation.IOperation;
import org.eclipse.basyx.vab.protocol.api.IConnectorFactory;
import org.eclipse.basyx.vab.protocol.http.connector.HTTPConnectorFactory;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RfidMqttCallback implements org.eclipse.paho.client.mqttv3.MqttCallback {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RfidMqttCallback.class);
    private RfidReader rfid;
    private final AASRegistryProxy registryProxy;
	private ConnectedAssetAdministrationShellManager manager;
    private String displayUrn;

    public RfidMqttCallback(RfidReader rfid, String displayUrn){
        this.rfid = rfid;
        this.registryProxy = new AASRegistryProxy(Common.getRegistryPath());
        this.manager = new ConnectedAssetAdministrationShellManager(registryProxy);
        this.displayUrn = displayUrn;
    }

    @Override
    public void connectionLost(Throwable throwable) {
        logger.debug("Connection lost. " + throwable);
        
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        logger.debug("deliveryComplete: {}",  iMqttDeliveryToken);
        
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        logger.debug("topic: {}, message: {}", topic, mqttMessage);
        
        Map<String, Object> map = new HashMap<>();

        //set lastResult
        try{
            String msg = mqttMessage.toString();
            msg = msg.replace("\n", "");
        
            ObjectMapper mapper = new ObjectMapper();
            map = mapper.readValue(msg, HashMap.class);
            rfid.setLastResult(map);
        }
        catch(Exception e){
            logger.debug("Could not update value of lastResult: ", e);
        }

        //display lastResult on Lcd display
        try{
            IConnectorFactory connectorFactory = new HTTPConnectorFactory();
            this.manager = new ConnectedAssetAdministrationShellManager(registryProxy, connectorFactory);

            ModelUrn aasURN = new ModelUrn(displayUrn);
            ConnectedAssetAdministrationShell connectedAAS = manager.retrieveAAS(aasURN);

            Map<String, ISubmodel> submodels = connectedAAS.getSubmodels();
            ISubmodel connectedSM = submodels.get("display");
            IOperation operation = connectedSM.getOperations().get("displayText");

            String chipText = map.get("chip_text").toString();
            chipText = chipText.trim();
            if(chipText.isEmpty() || chipText.isBlank()){
                chipText = "null";
            }
            operation.invoke(chipText + "," + map.get("chip_id"));
        }
        catch(Exception e){
            logger.debug("Could not display value of lastResult: ", e);
        }      
    }
}
