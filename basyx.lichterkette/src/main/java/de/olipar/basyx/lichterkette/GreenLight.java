/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.io.IOException;

import org.slf4j.LoggerFactory;

public class GreenLight implements ILight {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(GreenLight.class);

	private String id;
	private int gpioPort = -1;

	public GreenLight(String id) {
		this.id = id;
	}

	public GreenLight(String id, int gpioPort) {
		this.id = id;
		this.gpioPort = gpioPort;
	}

	@Override
	public void activate() {
		if (gpioPort >= 0) {
			try {
				// the Red/Green LEDs used are center positive, so ground is switched
				// ie. Digital low -> LED on
				Runtime.getRuntime().exec("raspi-gpio set " + gpioPort + " dl").waitFor();
			} catch (IOException | InterruptedException e) {
				logger.warn("Could not set digital low on port " + gpioPort + ": " + e.getMessage());
			}
		}
		logger.info("Green-" + id + " activated");
	}

	@Override
	public void deactivate() {
		if (gpioPort >= 0) {
			try {
				Runtime.getRuntime().exec("raspi-gpio set " + gpioPort + " dh").waitFor();
			} catch (IOException | InterruptedException e) {
				logger.warn("Could not set digital high on port " + gpioPort + ": " + e.getMessage());
			}
		}

		logger.info("Green-" + id + " deactivated");
	}

	public String getID() {
		return id;
	}
}
