/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.LoggerFactory;

public class LcdDisplay implements IDisplay {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LcdDisplay.class);

    private final String id;
    public List<Process> processes = new ArrayList<Process>();

    public LcdDisplay(String id){
        this.id = id;
    }

    @Override
    public boolean displayText(String text) {
        try{
            logger.info("Trying to display text: " + text);
            publishMqtt("display/Lcd_0/text", text);

            return true;
        }
        catch (Exception e){
            logger.debug("Displaying text failed: " + e);
        }
        return false;
    }

    @Override
    public boolean cycleSensorValues() {
        try{
            logger.info("Start of cycling sensor values");            
            publishMqtt("display/Lcd_0/cycle", " ");
            
            return true;
        }
        catch (Exception e){
            logger.debug("Start of cycling sensor values failed: " + e);
        }
        return false;
    }

    private void publishMqtt(String topic, String msg){
        try{
            MqttClient mqttClient = new MqttClient(Common.getMQTTBrokerHost(), "DisplayController");
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setAutomaticReconnect(true);
            mqttClient.connect(connOpts);
            MqttMessage message = new MqttMessage(msg.getBytes());
            message.setQos(0);
            mqttClient.publish(topic, message);
        }
        catch(Exception e){
            logger.debug("MQTT connection failed: " + e);
        }
    }

    public String getID() {
        return id;
    }
}
