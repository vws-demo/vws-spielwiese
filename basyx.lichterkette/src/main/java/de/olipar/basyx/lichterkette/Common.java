/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.slf4j.LoggerFactory;

public class Common {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Common.class);

	public static String getHostAddress() {
		try {
			return InetAddress.getLocalHost().getHostName() + ".local";
		} catch (UnknownHostException e) {
			logger.warn("Local hostname could not be found. Using loopback address (127.0.0.1) as fallback.");
			e.printStackTrace();
			return "127.0.0.1";
		}
	}

	public static int getHostPort() {
		String uncheckedPort = System.getenv("PORT");
		if (uncheckedPort == null || uncheckedPort.isEmpty()) {
			logger.warn("No valid PORT set.");
			logger.warn("You might want to explicitly set a PORT environment variable, eg:");
			logger.warn("$ export PORT=4000");
			logger.warn("A port 4000 will be assumed going forward.");
			return 4000;
		} else {
			try {
				return Integer.parseUnsignedInt(uncheckedPort);
			} catch (NumberFormatException e) {
				logger.warn("No valid PORT set.");
				logger.warn("You might want to explicitly set a PORT environment variable, eg:");
				logger.warn("$ export PORT=4000");
				logger.warn("A port 4000 will be assumed going forward.");
				return 4000;
			}
		}
	}

	public static String getRegistryPath() {
		String uncheckedRegistryPath = System.getenv("REGISTRYPATH");
		if (uncheckedRegistryPath == null || uncheckedRegistryPath.isEmpty()) {
			logger.warn("No valid REGISTRYPATH set.");
			logger.warn("You might want to explicitly set a REGISTRYPATH environment variable, eg:");
			logger.warn("$ export REGISTRYPATH=\"http://registry.local:4000/registry\"");
			logger.warn("A registry endpoint at http://127.0.0.1:4000/registry will be assumed going forward.");
			return "http://127.0.0.1:4000/registry";
		} else {
			return uncheckedRegistryPath;
		}
	}

	public static String getAASServerPath() {
		String uncheckedAASServerPath = System.getenv("AASSERVERPATH");
		if (uncheckedAASServerPath == null || uncheckedAASServerPath.isEmpty()) {
			logger.warn("No valid AASSERVERPATH set.");
			logger.warn("You might want to explicitly set a AASSERVERPATH environment variable, eg:");
			logger.warn("$ export AASSERVERPATH=\"http://aasserver.local:4001/aasServer\"");
			logger.warn("An AAS server endpoint at http://127.0.0.1:4001/aasServer will be assumed going forward.");
			return "http://localhost:4001/aasServer";
		} else {
			return uncheckedAASServerPath;
		}
	}

	public static String getMQTTBrokerHost() {
		String uncheckedMQTTBrokerHost = System.getenv("MQTTBROKERHOST");
		if (uncheckedMQTTBrokerHost == null || uncheckedMQTTBrokerHost.isEmpty()) {
			logger.warn("No valid MQTTBROKERHOST set.");
			logger.warn("You might want to explicitly set a MQTTBROKERHOST environment variable, eg:");
			logger.warn("$ export MQTTBROKERHOST=\"tcp://esp-controller.local:1883\"");
			logger.warn("An MQTT broker at tcp://esp-controller.local:1883 will be assumed going forward.");
			return "tcp://esp-controller.local:1883";
		} else {
			return "tcp://" + uncheckedMQTTBrokerHost + ":1883";
		}
	}

	public static boolean inDocker(){
		try
        {
            String inDocker = System.getenv("IN_DOCKER");
            if(inDocker == null){return false;}
            return true;
        }
        catch(Exception e){
            return false;
        }
	}
}
