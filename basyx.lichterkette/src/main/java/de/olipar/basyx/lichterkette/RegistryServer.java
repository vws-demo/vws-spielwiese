/*******************************************************************************
 * Copyright (C) 2022 the Eclipse BaSyx Authors, modified by Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package de.olipar.basyx.lichterkette;

import org.eclipse.basyx.components.configuration.BaSyxContextConfiguration;
import org.eclipse.basyx.components.registry.RegistryComponent;
import org.eclipse.basyx.components.registry.configuration.BaSyxRegistryConfiguration;
import org.eclipse.basyx.components.registry.configuration.RegistryBackend;
import org.slf4j.LoggerFactory;

/**
 * This class starts an AAS server and a Registry server
 * 
 * Based in part on various examples and snippets provided by the Eclipse BaSyx
 * Authors
 * 
 * @author schnicke, conradi, olipar
 *
 */
public class RegistryServer {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RegistryServer.class);

	public static void main(String[] args) {
		final int HOSTPORT = Common.getHostPort();
		logger.info("Local host port is set to " + HOSTPORT);
		
		startRegistry(HOSTPORT);
	}

	/**
	 * Starts an empty registry at "http://localhost:{HOSTPORT}"
	 */
	private static void startRegistry(final int HOSTPORT) {
		BaSyxContextConfiguration contextConfig = new BaSyxContextConfiguration(HOSTPORT, "/registry");
		BaSyxRegistryConfiguration registryConfig = new BaSyxRegistryConfiguration(RegistryBackend.INMEMORY);
		RegistryComponent registry = new RegistryComponent(contextConfig, registryConfig);

		// Start the created server
		registry.startComponent();
	}
}
