/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.basyx.aas.manager.ConnectedAssetAdministrationShellManager;
import org.eclipse.basyx.aas.metamodel.map.descriptor.AASDescriptor;
import org.eclipse.basyx.aas.metamodel.map.descriptor.SubmodelDescriptor;
import org.eclipse.basyx.aas.registration.proxy.AASRegistryProxy;
import org.eclipse.basyx.submodel.metamodel.api.ISubmodel;
import org.eclipse.basyx.submodel.metamodel.api.submodelelement.operation.IOperation;
import org.eclipse.basyx.vab.exception.provider.ResourceNotFoundException;
import org.slf4j.LoggerFactory;

/**
 * Light controller. Controls lighting of all red and green lights found.
 * 
 * @author olipar
 *
 */
public class LightController implements ILightController {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LightController.class);

	private boolean isActive = false;
	private boolean isRedOn = false;
	private boolean isGreenOn = false;
	private boolean isBlinking = false;
	private boolean isDeactivating = false;
	private final double updatePeriod = 1.0D;
	private final AASRegistryProxy registry;
	private ConnectedAssetAdministrationShellManager manager;
	private List<ISubmodel> redLightSwitches = new LinkedList<ISubmodel>();
	private List<ISubmodel> greenLightSwitches = new LinkedList<ISubmodel>();
	private boolean isStale = true;

	public LightController(final AASRegistryProxy registry) {
		this.registry = registry;
		this.manager = new ConnectedAssetAdministrationShellManager(registry);

		new Thread(() -> {
			while (true) {
				try {
					Thread.sleep((long) (updatePeriod * 1000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (isActive) {
					if (isStale) {
						logger.info("Light list of this controller is stale. Updating...");
						this.updateLampList();
						isStale = false;
					}
					if (isBlinking) {
						isRedOn = !isRedOn;
						isGreenOn = !isGreenOn;
					}
					
					for (ISubmodel light : greenLightSwitches) {
						try {
							if (isGreenOn) {
								((IOperation) light.getSubmodelElement("activateLight")).invoke();
							} else {
								((IOperation) light.getSubmodelElement("deactivateLight")).invoke();
							}
						} catch (ResourceNotFoundException e) {
							if (!isStale) {
								logger.warn(
										"Invoking a light switch failed. Lights will be updated on next update while active.");
							}
							isStale = true;
						}
					}
					for (ISubmodel light : redLightSwitches) {
						try {
							if (isRedOn) {
								((IOperation) light.getSubmodelElement("activateLight")).invoke();
							} else {
								((IOperation) light.getSubmodelElement("deactivateLight")).invoke();
							}
						} catch (ResourceNotFoundException e) {
							if (!isStale) {
								logger.warn(
										"Invoking a light switch failed. Lights will be updated on next update while active.");
							}
							isStale = true;
						}
					}

					if (isDeactivating) {
						isActive = false;
						isDeactivating = false;
						logger.info("Light controller deactivated");
					}
				}
			}
		}).start();
	}

	private void activate() {
		if (!isActive) {
			logger.info("Light controller activated");
			isActive = true;
		}
	}

	public void deactivate() {
		if (isActive) {
			isDeactivating = true;
			isRedOn = false;
			isGreenOn = false;
			isBlinking = false;
		}
	}

	public boolean isActive() {
		return isActive;
	}

	@Override
	public void redLight() {
		activate();
		isGreenOn = false;
		isRedOn = true;
		isBlinking = false;
	}

	@Override
	public void greenLight() {
		activate();
		isGreenOn = true;
		isRedOn = false;
		isBlinking = false;
	}

	@Override
	public void blinkLight() {
		activate();
		// we need to start some light to alternate later
		isGreenOn = true;
		isRedOn = false;
		isBlinking = true;
	}

	@Override
	public void staticLight() {
		activate();
		isGreenOn = true;
		isRedOn = true;
		isBlinking = false;
	}

	@Override
	public boolean isRedOn() {
		return isRedOn;
	}

	@Override
	public boolean isGreenOn() {
		return isGreenOn;
	}

	@Override
	public void updateLamps() {
		isStale = true;
	}

	public void updateLampList() {
		redLightSwitches.clear();
		greenLightSwitches.clear();
		for (AASDescriptor iter : registry.lookupAll()) {
			if (iter.getIdShort().contains("Light")) {
				for (SubmodelDescriptor lightModel : iter.getSubmodelDescriptors()) {
					if (lightModel.getIdShort().equals("lightSwitch")) {
						ISubmodel submodel = manager.retrieveSubmodel(iter.getIdentifier(), lightModel.getIdentifier());
						if (iter.getIdShort().contains("red")) {
							redLightSwitches.add(submodel);
						} else if (iter.getIdShort().contains("green")) {
							greenLightSwitches.add(submodel);
						}
					}
				}
			}
		}
	}
}