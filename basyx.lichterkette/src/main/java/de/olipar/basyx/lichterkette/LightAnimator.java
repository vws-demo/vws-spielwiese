/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.basyx.aas.manager.ConnectedAssetAdministrationShellManager;
import org.eclipse.basyx.aas.metamodel.map.descriptor.AASDescriptor;
import org.eclipse.basyx.aas.metamodel.map.descriptor.SubmodelDescriptor;
import org.eclipse.basyx.aas.registration.proxy.AASRegistryProxy;
import org.eclipse.basyx.submodel.metamodel.api.ISubmodel;
import org.eclipse.basyx.submodel.metamodel.api.submodelelement.operation.IOperation;
import org.eclipse.basyx.vab.exception.provider.ResourceNotFoundException;
import org.slf4j.LoggerFactory;

/**
 * Light animator. Controls animations via all found LightControllers.
 *
 * @author olipar
 *
 */
public class LightAnimator implements ILightAnimator {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LightAnimator.class);
	
	private boolean isActive = false;
	private boolean isStaticLight = false;
	private boolean isAnimation1 = false;
	private boolean isAnimation2 = false;
	private int animationState = 0;
	private final double updatePeriod = 3.0D;
	private final AASRegistryProxy registry;
	private ConnectedAssetAdministrationShellManager manager;
	private List<ISubmodel> lightControllers = new LinkedList<ISubmodel>();
	private boolean isStale = true;

	public LightAnimator(final AASRegistryProxy registry) {
		this.registry = registry;
		this.manager = new ConnectedAssetAdministrationShellManager(registry);

		new Thread(() -> {
			while (true) {
				try {
					Thread.sleep((long) (updatePeriod * 1000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (isActive) {
					if (isStale) {
						logger.info("Controller list of this animator is stale. Updating...");
						this.updateControllerList();
						isStale = false;
					}
					try {
						if (isStaticLight) {
							if (animationState == 0) {
								allLights();
								animationState++;
							}
						}
						if (isAnimation1) {
							switch (animationState) {
							case 0:
								allLights();
								animationState++;
								break;
							case 1:
								redLight();
								animationState++;
								break;
							case 2:
								greenLight();
								animationState = 0;
								break;
							}
						}
						if (isAnimation2) {
							switch (animationState) {
							case 0:
								blinkLight();
								animationState++;
								break;
							case 1:
								allLights();
								animationState = 0;
								break;
							}
						}
					} catch (ResourceNotFoundException e) {
						if (!isStale) {
							logger.warn(
									"Invoking a controller failed. List will be updated on next update while active.");
						}
						isStale = true;
					}
				}

			}
		}).start();
	}

	private void activate() {
		if (!isActive) {
			logger.info("Light animator activated");
			isActive = true;
		}
	}

	private void redLight() {
		for (ISubmodel controller : lightControllers) {
			((IOperation) controller.getSubmodelElement("redLight")).invoke();
		}
	}

	private void greenLight() {
		for (ISubmodel controller : lightControllers) {
			((IOperation) controller.getSubmodelElement("greenLight")).invoke();
		}
	}

	private void allLights() {
		for (ISubmodel controller : lightControllers) {
			((IOperation) controller.getSubmodelElement("staticLight")).invoke();
		}
	}

	private void blinkLight() {
		for (ISubmodel controller : lightControllers) {
			((IOperation) controller.getSubmodelElement("blinkLight")).invoke();
		}

	}

	@Override
	public void updateControllers() {
		isStale = true;
	}

	public void updateControllerList() {
		lightControllers.clear();
		for (AASDescriptor iter : registry.lookupAll()) {
			if (iter.getIdShort().contains("lightController")) {
				for (SubmodelDescriptor controllerModel : iter.getSubmodelDescriptors()) {
					if (controllerModel.getIdShort().equals("lightControllerSM")) {
						ISubmodel submodel = manager.retrieveSubmodel(iter.getIdentifier(),
								controllerModel.getIdentifier());
						lightControllers.add(submodel);
					}
				}
			}
		}
	}

	@Override
	public void animation1() {
		activate();
		isAnimation1 = true;
		isAnimation2 = false;
		isStaticLight = false;
		animationState = 0;
	}

	@Override
	public void animation2() {
		activate();
		isAnimation1 = false;
		isAnimation2 = true;
		isStaticLight = false;
		animationState = 0;
	}

	@Override
	public void staticLight() {
		activate();
		isAnimation1 = false;
		isAnimation2 = false;
		isStaticLight = true;
		animationState = 0;
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	@Override
	public void deactivate() {
		if (isActive) {
			for (ISubmodel controller : lightControllers) {
				((IOperation) controller.getSubmodelElement("deactivateLights")).invoke();
			}
			animationState = 0;
			isAnimation1 = false;
			isAnimation2 = false;
			isStaticLight = false;
			isActive = false;
			logger.info("Light animator deactivated");
		}
	}
}