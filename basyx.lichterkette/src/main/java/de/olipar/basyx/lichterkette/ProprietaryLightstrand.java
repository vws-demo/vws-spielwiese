/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.LoggerFactory;

/**
 * Proprietary Lightstrand. Creates red and green proprietary lights, according
 * to environment variable. uses the hostUploadAndRegister method from
 * LightSwitchSubmodelProvider to create, register and upload the AAS as well as
 * host and register the LightSwitch Submodules
 *
 * @author olipar
 *
 */

public class ProprietaryLightstrand {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ProprietaryLightstrand.class);
	private static int redLights;
	private static int greenLights;

	private static int getLightAmount(String color) {
		color = color.toUpperCase();
		String uncheckedAmount = System.getenv(color + "_LIGHTS");
		if (uncheckedAmount == null || uncheckedAmount.isEmpty()) {
			logger.warn("No valid amount for " + color.toLowerCase() + " lamps set.");
			logger.warn("You might want to explicitly set a " + color + "_LIGHTS environment variable, eg:");
			logger.warn("$ export " + color + "_LIGHTS=4");
			logger.warn("No " + color.toLowerCase() + " lights will be created.");
			return 0;
		}
		try {
			int uncheckedIntegerAmount = Integer.parseUnsignedInt(uncheckedAmount);
			if (uncheckedIntegerAmount < 0) {
				throw new IllegalArgumentException(color + "_LIGHTS is negative");
			}
			return uncheckedIntegerAmount;
		} catch (IllegalArgumentException e) {
			logger.warn("No valid amount for " + color.toLowerCase() + " lamps set.");
			logger.warn("Make sure " + color.toLowerCase() + " is a positive or zero integer value, eg:");
			logger.warn("$ export " + color + "_LIGHTS=4");
			logger.warn("No " + color.toLowerCase() + " lights will be created.");
			return 0;
		}
	}

	public static void main(String[] args) {

		redLights = getLightAmount("red");
		greenLights = getLightAmount("green");

		final int HOSTPORT = Common.getHostPort();
		logger.info("Local host port is set to " + HOSTPORT);
		logger.info("Ports " + HOSTPORT + " through including " + (HOSTPORT + redLights + greenLights - 1)
				+ " will be used for lights.");
		MqttClient mqttClient = null;
		try {
			mqttClient = new MqttClient(Common.getMQTTBrokerHost(), "ProprietaryLightstrand",
					new MemoryPersistence());
		} catch (MqttException e) {
			logger.warn("MQTT client could not be created: " + e.getMessage());
			logger.warn("No lights will be switched in the real world.");
		}
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		connOpts.setAutomaticReconnect(true);
		logger.info("Connecting to MQTT broker");
		try {
			mqttClient.connect(connOpts);
		} catch (NullPointerException | MqttException e) {
			logger.warn("MQTT client could not connect to broker: " + e.getMessage());
			logger.warn("No lights will be switched in the real world.");
		}

		for (int i = 0; i < redLights; i++) {
			RedProprietaryLight light;
			if (mqttClient != null) {
				light = new RedProprietaryLight("ProprietaryLight" + i, mqttClient);
			} else {
				light = new RedProprietaryLight("ProprietaryLight" + i);
			}

			LightSwitchSubmodelProvider lssp = new LightSwitchSubmodelProvider(
					"ProprietaryLightstrand:redLight_" + light.getID());
			lssp.hostUploadAndRegister(light, "redLight_" + light.getID(), "redLight_" + light.getID(), HOSTPORT + i);
		}
		for (int i = 0; i < greenLights; i++) {
			GreenProprietaryLight light;
			if (mqttClient != null) {
				light = new GreenProprietaryLight("ProprietaryLight" + i, mqttClient);
			} else {
				light = new GreenProprietaryLight("ProprietaryLight" + i);
			}

			LightSwitchSubmodelProvider lssp = new LightSwitchSubmodelProvider(
					"ProprietaryLightstrand:greenLight_" + light.getID());
			lssp.hostUploadAndRegister(light, "greenLight_" + light.getID(), "greenLight_" + light.getID(),
					HOSTPORT + redLights + i);
		}
	}

}
