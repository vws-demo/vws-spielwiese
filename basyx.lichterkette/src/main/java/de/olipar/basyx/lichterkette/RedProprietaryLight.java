/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.LoggerFactory;

public class RedProprietaryLight implements ILight {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RedProprietaryLight.class);

	private String proprietaryId;
	private MqttClient client = null;
	private static String topicPrefix = "proprietaryProtocol/";

	public RedProprietaryLight(String proprietaryId) {
		this.proprietaryId = proprietaryId;
	}

	public RedProprietaryLight(String proprietaryId, MqttClient client) {
		this.proprietaryId = proprietaryId;
		this.client = client;
	}

	@Override
	public void activate() {
		if (client != null) {
			try {
				MqttMessage message = new MqttMessage();
				message.setQos(0);
				client.publish(topicPrefix + proprietaryId + "/red/on", message);
				logger.info("Red-" + proprietaryId + " activated");
			} catch (MqttException e) {
				logger.warn("Could not activate red light on " + proprietaryId + ": " + e.getMessage());
			}
		} else {
			logger.info("Red-" + proprietaryId + " activated");
		}
	}

	@Override
	public void deactivate() {
		if (client != null) {
			try {
				MqttMessage message = new MqttMessage();
				message.setQos(0);
				client.publish(topicPrefix + proprietaryId + "/red/off", message);
				logger.info("Red-" + proprietaryId + " deactivated");
			} catch (MqttException e) {
				logger.warn("Could not deactivate red light on " + proprietaryId + ": " + e.getMessage());
			}
		} else {
			logger.info("Red-" + proprietaryId + " deactivated");
		}
	}

	public String getID() {
		return proprietaryId;
	}
}
