/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import org.slf4j.LoggerFactory;

import com.google.common.base.Supplier;

import org.eclipse.basyx.aas.metamodel.map.descriptor.ModelUrn;
import org.eclipse.basyx.submodel.metamodel.api.qualifier.haskind.ModelingKind;
import org.eclipse.basyx.submodel.metamodel.map.Submodel;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.Operation;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.OperationVariable;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.AASLambdaPropertyHelper;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.Property;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.valuetype.ValueType;
import java.util.Arrays;
import java.util.function.Function;


/**
 * Rfid submodel provider. Can create an AAS and Submodel for given
 * Rfid reader. Can Upload the AAS, host the active Submodel and register it
 * <p>
 * Based in parts on various examples and snippets provided by the Eclipse BaSyx
 * Authors
 *
 * @author peggy
 */

public class RfidSubmodelProvider extends GeneralSubmodelProvider {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RfidSubmodelProvider.class);
    private final String uniqueID;

    public RfidSubmodelProvider(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public Submodel createRfidModel(IRfidReader rfid) {
        // Create an empty Submodel
        Submodel sensorSubmodel = new Submodel();

        // Set its idShort
        sensorSubmodel.setIdShort("sensor");

        // Set its unique identification
        sensorSubmodel.setIdentification(new ModelUrn("Sensor" + uniqueID));

        // Add a function that reads the value from the sensor and implements a functional interface
        Function<Object[], Object> readTagFunction = (args) -> rfid.readTag();
        Operation readTagOperation = new Operation(readTagFunction);
        readTagOperation.setIdShort("readTag");
        sensorSubmodel.addSubmodelElement(readTagOperation);

        // Add a function that starts a rfid reading loop and implements a functional interface
        Function<Object[], Object> readTagLoopFunction = (args) -> rfid.startReadTagLoop();
        Operation readTagLoopOperation = new Operation(readTagLoopFunction);
        readTagLoopOperation.setIdShort("startReadTagLoop");
        sensorSubmodel.addSubmodelElement(readTagLoopOperation);

        // Add a function that allows to write text to a rfid tag and implements a functional interface
        Property input = new Property("rfidInput");
        input.setModelingKind(ModelingKind.TEMPLATE);
        input.setValueType(ValueType.String);

        Function<Object[], Object> writeTagFunction = (args) -> rfid.writeTag(args[0].toString());
        Operation writeTagOperation = new Operation(writeTagFunction);
        writeTagOperation.setIdShort("writeRfid");
        writeTagOperation.setInputVariables(Arrays.asList(new OperationVariable(input)));
        sensorSubmodel.addSubmodelElement(writeTagOperation);

        // Add a function to get lastResult and implements a functional interface
        Function<Object[], Object> getLastResultFunction = (args) -> rfid.getLastResult();
        Operation getLastResultOperation = new Operation(getLastResultFunction);
        getLastResultOperation.setIdShort("getLastResult");
        sensorSubmodel.addSubmodelElement(getLastResultOperation);

        // Add a property that contains the last read result
        Property lastResult = new Property("lastResult", "{}");
        Supplier<Object> readLastResultFunction = () -> rfid.getLastResult().toString();
        AASLambdaPropertyHelper.setLambdaValue(lastResult, readLastResultFunction, null);

        //Consumer<Object> writeFunction = (text) -> {lastResult.setValue(text); };
        //AASLambdaPropertyHelper.setLambdaValue(lastResult, null, writeFunction);

        sensorSubmodel.addSubmodelElement(lastResult);

        // Return the Submodel
        return sensorSubmodel;
    }

    public void hostUploadAndRegister(IRfidReader rfid, String AASID, String AASShortID, int port) {

        // First, a local model is created that is wrapped by a model provider
        Submodel sensorModel = createRfidModel(rfid);

        super.hostUploadAndRegister(sensorModel, AASID, AASShortID, port);
    }
}
