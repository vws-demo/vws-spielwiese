/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import org.slf4j.LoggerFactory;

 /**
  * LcdController. Creates 1 lcd display, uses the
  * hostUploadAndRegister method from DisplaySubmodelProvider to create,
  * register and upload the AAS as well as host and register the Rfid
  * Submodules
  *
  * @author peggy
  */
public class LcdDisplayController {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RfidController.class);
    private static int displayAmount;

    public static void main(String[] args) {
        final int HOSTPORT = Common.getHostPort();
        
        displayAmount = getAmount();
        
        for (int i = 0; i < displayAmount; i++) {
            LcdDisplay display = new LcdDisplay(Integer.toString(i));
            DisplaySubmodelProvider dsp = new DisplaySubmodelProvider("Display:Lcd_" + display.getID());
            dsp.hostUploadAndRegister(display, "Lcd_"+display.getID(), "Lcd_"+display.getID(), HOSTPORT+i);
            display.cycleSensorValues();
        }
    }

    private static int getAmount(){
        String amountString = System.getenv("DISPLAYAMOUNT");

        if (amountString == null || amountString.isEmpty()) {
            logger.warn("No valid amount for displays set.");
            logger.warn("You might want to explicitly set a DISPLAYAMOUNT environment variable.");
            return 0;
        }

        try {
            int amountInt = Integer.parseUnsignedInt(amountString);
            if (amountInt < 0) {
                throw new IllegalArgumentException("DISPLAYAMOUNT is negative");
            }
            return amountInt;
        } catch (IllegalArgumentException e) {
            logger.warn("No valid amount for DISPLAYAMOUNT sensors set. No display will be created.");
            return 0;
        }
    }
}
