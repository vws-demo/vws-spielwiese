/*******************************************************************************
 * Copyright (C) 2022 the Eclipse BaSyx Authors, Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.util.function.Function;
import java.util.function.Supplier;

import javax.servlet.http.HttpServlet;

import org.eclipse.basyx.aas.manager.ConnectedAssetAdministrationShellManager;
import org.eclipse.basyx.aas.metamodel.api.parts.asset.AssetKind;
import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell;
import org.eclipse.basyx.aas.metamodel.map.descriptor.ModelUrn;
import org.eclipse.basyx.aas.metamodel.map.descriptor.SubmodelDescriptor;
import org.eclipse.basyx.aas.metamodel.map.parts.Asset;
import org.eclipse.basyx.aas.registration.proxy.AASRegistryProxy;
import org.eclipse.basyx.submodel.metamodel.map.Submodel;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.AASLambdaPropertyHelper;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.Property;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.Operation;
import org.eclipse.basyx.submodel.restapi.SubmodelProvider;
import org.eclipse.basyx.vab.modelprovider.api.IModelProvider;
import org.eclipse.basyx.vab.protocol.http.server.BaSyxContext;
import org.eclipse.basyx.vab.protocol.http.server.BaSyxHTTPServer;
import org.eclipse.basyx.vab.protocol.http.server.VABHTTPInterface;
import org.slf4j.LoggerFactory;

/**
 * Light controller submodel provider. Creates an AAS and Submodel for a created
 * LightController. Uploads the AAS, hosts the active Submodel and registers it
 * 
 * Based in part on various examples and snippets provided by the Eclipse BaSyx
 * Authors
 * 
 * @author olipar, conradi, kuhn
 *
 */
public class LightControllerSubmodelProvider {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LightControllerSubmodelProvider.class);

	public static Submodel createMyLightControllerModel(ILightController lightController) {
		// Create an empty Submodel
		Submodel lightControllerSubmodel = new Submodel();

		// Set its idShort
		lightControllerSubmodel.setIdShort("lightControllerSM");

		// Set its unique identification
		lightControllerSubmodel.setIdentification(new ModelUrn("lightControllerSM1"));

		// Now we want to create a dynamic property that can resolve its value during
		// runtime
		// 1. Create a supplier function that can determine the green lights state
		Supplier<Object> lambdaGreenReadFunction = () -> lightController.isGreenOn();
		// 2. Create a new empty Property
		Property dynamicGreenLightProperty = new Property();
		// 3. Set the id of the new Property
		dynamicGreenLightProperty.setIdShort("isGreenOn");
		// 4. Use the AASLambdaPropertyHelper to add the Getter to the new Property
		// NOTE: A setter function is not required (=> null), because a light state
		// getter is "read only"
		AASLambdaPropertyHelper.setLambdaValue(dynamicGreenLightProperty, lambdaGreenReadFunction, null);
		// 5. Add that lambda property to the model
		lightControllerSubmodel.addSubmodelElement(dynamicGreenLightProperty);

		// Do the same for all other readable values
		Supplier<Object> lambdaRedReadFunction = () -> lightController.isRedOn();
		Property dynamicRedLightProperty = new Property();
		dynamicRedLightProperty.setIdShort("isRedOn");
		AASLambdaPropertyHelper.setLambdaValue(dynamicRedLightProperty, lambdaRedReadFunction, null);
		lightControllerSubmodel.addSubmodelElement(dynamicRedLightProperty);

		Supplier<Object> lambdaActivenessReadFunction = () -> lightController.isActive();
		Property dynamicActivenessProperty = new Property();
		dynamicActivenessProperty.setIdShort("isActive");
		AASLambdaPropertyHelper.setLambdaValue(dynamicActivenessProperty, lambdaActivenessReadFunction, null);
		lightControllerSubmodel.addSubmodelElement(dynamicActivenessProperty);

		// Add a function that deactivates the light controller and implements a
		// functional interface
		Function<Object[], Object> deactivateFunction = (args) -> {
			lightController.deactivate();
			return null;
		};
		// Encapsulate the function in an operation
		Operation deactivateOperation = new Operation(deactivateFunction);
		// Set the id of the operation
		deactivateOperation.setIdShort("deactivateLights");
		// Add an operation that deactivates the light controller and implements a
		// functional interface
		lightControllerSubmodel.addSubmodelElement(deactivateOperation);

		// Do the same for every other non-getter function in ILightController
		Function<Object[], Object> updateLampsFunction = (args) -> {
			lightController.updateLamps();
			return null;
		};
		Operation updateLampsOperation = new Operation(updateLampsFunction);
		updateLampsOperation.setIdShort("updateLamps");
		lightControllerSubmodel.addSubmodelElement(updateLampsOperation);

		Function<Object[], Object> staticLightFunction = (args) -> {
			lightController.staticLight();
			return null;
		};
		Operation staticLightOperation = new Operation(staticLightFunction);
		staticLightOperation.setIdShort("staticLight");
		lightControllerSubmodel.addSubmodelElement(staticLightOperation);

		Function<Object[], Object> blinkLightFunction = (args) -> {
			lightController.blinkLight();
			return null;
		};
		Operation blinkLightOperation = new Operation(blinkLightFunction);
		blinkLightOperation.setIdShort("blinkLight");
		lightControllerSubmodel.addSubmodelElement(blinkLightOperation);

		Function<Object[], Object> redLightFunction = (args) -> {
			lightController.redLight();
			return null;
		};
		Operation redLightOperation = new Operation(redLightFunction);
		redLightOperation.setIdShort("redLight");
		lightControllerSubmodel.addSubmodelElement(redLightOperation);

		Function<Object[], Object> greenLightFunction = (args) -> {
			lightController.greenLight();
			return null;
		};
		Operation greenLightOperation = new Operation(greenLightFunction);
		greenLightOperation.setIdShort("greenLight");
		lightControllerSubmodel.addSubmodelElement(greenLightOperation);

		// Return the Submodel
		return lightControllerSubmodel;
	}

	public static void main(String[] args) {
		final String HOSTADDRESS = Common.getHostAddress();
		logger.info("Local host address is set to " + HOSTADDRESS);
		final int HOSTPORT = Common.getHostPort();
		logger.info("Local host port is set to " + HOSTPORT);
		final String REGISTRYPATH = Common.getRegistryPath();
		logger.info("Registry path set to " + REGISTRYPATH);
		final String AASSERVERPATH = Common.getAASServerPath();
		logger.info("AAS server path set to " + AASSERVERPATH);

		AASRegistryProxy registry = new AASRegistryProxy(REGISTRYPATH);

		// First, a local model is created that is wrapped by a model provider
		Submodel lightControllerModel = createMyLightControllerModel(new LightController(registry));
		// Now wrap the model in a SubmodelProvider
		IModelProvider modelProvider = new SubmodelProvider(lightControllerModel);

		// Now, create the servlet that will provide the http/REST interface for
		// accessing the light controller Submodel
		// => Every servlet that is provided by this node is available at
		// http://{HOSTADDRESS}:{HOSTPORT}/handson/
		BaSyxContext context = new BaSyxContext("/handson", "", HOSTADDRESS, HOSTPORT);

		// Now, the model provider is attached to a HTTP servlet that enables access to
		// the model
		// in the next steps through a HTTP rest interface
		// => The model will be published using an HTTP-REST interface
		HttpServlet modelServlet = new VABHTTPInterface<IModelProvider>(modelProvider);
		logger.info("Created a servlet for the light controller model");

		// The provider will be available at
		// http://{HOSTADDRESS}:{HOSTPORT}/handson/lightController/
		// And Submodel can be accessed at:
		// http://{HOSTADDRESS}:{HOSTPORT}/handson/lightController/submodel
		context.addServletMapping("/lightController/*", modelServlet);

		// Start the local HTTP server
		BaSyxHTTPServer server = new BaSyxHTTPServer(context);
		server.start();
		logger.info("HTTP server started");

		// The URN is a unique name, and refers to the asset
		ModelUrn aasURN = new ModelUrn("urn:de.olipar.basyx:LightControllerAAS");

		// A digital representation of the asset
		Asset asset = new Asset("lightControllerAsset", aasURN, AssetKind.INSTANCE);

		// Create the Asset Administration Shell object
		AssetAdministrationShell aas = new AssetAdministrationShell("lightController", aasURN, asset);

		// Now add the references of the submodels to the AAS header
		aas.addSubmodel(lightControllerModel);

		// Explicitly create and add submodel descriptors
		SubmodelDescriptor lightControllerSMDescriptor = new SubmodelDescriptor(lightControllerModel,
				"http://" + HOSTADDRESS + ":" + HOSTPORT + "/handson/lightController/submodel");

		// Create a ConnectedAASManager using the registryProxy as its registry
		ConnectedAssetAdministrationShellManager manager = new ConnectedAssetAdministrationShellManager(registry);

		// The ConnectedAASManager automatically pushes the given AAS
		// to the server to which the address was given
		// It also registers the AAS in the registry
		manager.createAAS(aas, AASSERVERPATH);

		// Register the light controller Submodel at the registry
		registry.register(aas.getIdentification(), lightControllerSMDescriptor);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("Shutting down Light Controller...");
				manager.deleteSubmodel(aas.getIdentification(), lightControllerSMDescriptor.getIdentifier());
				manager.deleteAAS(aas.getIdentification());
			}
		});

	}

}
