/*******************************************************************************
 * Copyright (C) 2022 the Eclipse BaSyx Authors, Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.util.function.Function;
import java.util.function.Supplier;

import javax.servlet.http.HttpServlet;

import org.eclipse.basyx.aas.manager.ConnectedAssetAdministrationShellManager;
import org.eclipse.basyx.aas.metamodel.api.parts.asset.AssetKind;
import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell;
import org.eclipse.basyx.aas.metamodel.map.descriptor.ModelUrn;
import org.eclipse.basyx.aas.metamodel.map.descriptor.SubmodelDescriptor;
import org.eclipse.basyx.aas.metamodel.map.parts.Asset;
import org.eclipse.basyx.aas.registration.proxy.AASRegistryProxy;
import org.eclipse.basyx.submodel.metamodel.map.Submodel;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.AASLambdaPropertyHelper;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.Property;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.Operation;
import org.eclipse.basyx.submodel.restapi.SubmodelProvider;
import org.eclipse.basyx.vab.modelprovider.api.IModelProvider;
import org.eclipse.basyx.vab.protocol.http.server.BaSyxContext;
import org.eclipse.basyx.vab.protocol.http.server.BaSyxHTTPServer;
import org.eclipse.basyx.vab.protocol.http.server.VABHTTPInterface;
import org.slf4j.LoggerFactory;

/**
 * Light animator submodel provider. Creates an AAS and Submodel for a created
 * LightAnimator. Uploads the AAS, hosts the active Submodel and registers it
 * 
 * Based in part on various examples and snippets provided by the Eclipse BaSyx
 * Authors
 *
 * @author olipar, conradi, kuhn
 *
 */
public class LightAnimatorSubmodelProvider {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(LightAnimatorSubmodelProvider.class);

	public static Submodel createMyLightAnimatorModel(ILightAnimator lightAnimator) {
		// Create an empty Submodel
		Submodel lightAnimatorSubmodel = new Submodel();

		// Set its idShort
		lightAnimatorSubmodel.setIdShort("lightAnimatorSM");

		// Set its unique identification
		lightAnimatorSubmodel.setIdentification(new ModelUrn("lightAnimatorSM1"));

		// Now we want to create a dynamic property that can resolve its value during
		// runtime
		// 1. Create a supplier function that can determine whether the animator is
		// active
		Supplier<Object> lambdaActivenessReadFunction = () -> lightAnimator.isActive();
		// 2. Create a new empty Property
		Property dynamicActivenessProperty = new Property();
		// 3. Set the id of the new Property
		dynamicActivenessProperty.setIdShort("isActive");
		// 4. Use the AASLambdaPropertyHelper to add the Getter to the new Property
		// NOTE: A setter function is not required (=> null), because the animator state
		// is "read only"
		AASLambdaPropertyHelper.setLambdaValue(dynamicActivenessProperty, lambdaActivenessReadFunction, null);
		// 5. Add that lambda property to the model
		lightAnimatorSubmodel.addSubmodelElement(dynamicActivenessProperty);

		// Add a function that deactivates the animator and implements a functional
		// interface
		Function<Object[], Object> deactivateFunction = (args) -> {
			lightAnimator.deactivate();
			return null;
		};
		// Encapsulate the function in an operation
		Operation deactivateOperation = new Operation(deactivateFunction);
		// Set the id of the operation
		deactivateOperation.setIdShort("deactivateAnimator");
		// Add an operation that deactivates the animator and implements a functional
		// interface
		lightAnimatorSubmodel.addSubmodelElement(deactivateOperation);

		// Do the same for every other function in ILightAnimator
		Function<Object[], Object> updateControllersFunction = (args) -> {
			lightAnimator.updateControllers();
			return null;
		};
		Operation updateControllersOperation = new Operation(updateControllersFunction);
		updateControllersOperation.setIdShort("updateControllers");
		lightAnimatorSubmodel.addSubmodelElement(updateControllersOperation);

		Function<Object[], Object> staticLightFunction = (args) -> {
			lightAnimator.staticLight();
			return null;
		};
		Operation staticLightOperation = new Operation(staticLightFunction);
		staticLightOperation.setIdShort("staticLight");
		lightAnimatorSubmodel.addSubmodelElement(staticLightOperation);

		Function<Object[], Object> animation1Function = (args) -> {
			lightAnimator.animation1();
			return null;
		};
		Operation animation1Operation = new Operation(animation1Function);
		animation1Operation.setIdShort("animation1");
		lightAnimatorSubmodel.addSubmodelElement(animation1Operation);

		Function<Object[], Object> animation2Function = (args) -> {
			lightAnimator.animation2();
			return null;
		};
		Operation animation2Operation = new Operation(animation2Function);
		animation2Operation.setIdShort("animation2");
		lightAnimatorSubmodel.addSubmodelElement(animation2Operation);

		// Return the Submodel
		return lightAnimatorSubmodel;
	}

	public static void main(String[] args) {
		final String HOSTADDRESS = Common.getHostAddress();
		logger.info("Local host address is set to " + HOSTADDRESS);
		final int HOSTPORT = Common.getHostPort();
		logger.info("Local host port is set to " + HOSTPORT);
		final String REGISTRYPATH = Common.getRegistryPath();
		logger.info("Registry path set to " + REGISTRYPATH);
		final String AASSERVERPATH = Common.getAASServerPath();
		logger.info("AAS server path set to " + AASSERVERPATH);

		AASRegistryProxy registry = new AASRegistryProxy(REGISTRYPATH);

		// First, a local model is created that is wrapped by a model provider
		Submodel lightAnimatorModel = createMyLightAnimatorModel(new LightAnimator(registry));
		// Now wrap the model in a SubmodelProvider
		IModelProvider modelProvider = new SubmodelProvider(lightAnimatorModel);

		// Now, create the servlet that will provide the http/REST interface for
		// accessing the animator Submodel
		// => Every servlet that is provided by this node is available at
		// http://{HOSTADDRESS}:{HOSTPORT}/handson/
		BaSyxContext context = new BaSyxContext("/handson", "", HOSTADDRESS, HOSTPORT);

		// Now, the model provider is attached to a HTTP servlet that enables access to
		// the model
		// in the next steps through a HTTP rest interface
		// => The model will be published using an HTTP-REST interface
		HttpServlet modelServlet = new VABHTTPInterface<IModelProvider>(modelProvider);
		logger.info("Created a servlet for the light animator model");

		// The provider will be available at
		// http://HOSTADDRESS:{HOSTPORT}/handson/lightAnimator/
		// And the Submodel can be accessed at:
		// http://{HOSTADDRESS}:{HOSTPORT}/handson/lightAnimator/submodel
		context.addServletMapping("/lightAnimator/*", modelServlet);

		// Start the local HTTP server
		BaSyxHTTPServer server = new BaSyxHTTPServer(context);
		server.start();
		logger.info("HTTP server started");

		// The URN is a unique name, and refers to the asset
		ModelUrn aasURN = new ModelUrn("urn:de.olipar.basyx:LightAnimatorAAS");

		// A digital representation of the asset
		Asset asset = new Asset("lightAnimatorControllerAsset", aasURN, AssetKind.INSTANCE);

		// Create the Asset Administration Shell object
		AssetAdministrationShell aas = new AssetAdministrationShell("lightAnimator", aasURN, asset);

		// Now add the references of the submodels to the AAS header
		aas.addSubmodel(lightAnimatorModel);

		// Explicitly create and add submodel descriptors
		SubmodelDescriptor lightAnimatorSMDescriptor = new SubmodelDescriptor(lightAnimatorModel,
				"http://" + HOSTADDRESS + ":" + HOSTPORT + "/handson/lightAnimator/submodel");

		// Create a ConnectedAASManager using the registryProxy as its registry
		ConnectedAssetAdministrationShellManager manager = new ConnectedAssetAdministrationShellManager(registry);

		// The ConnectedAASManager automatically pushes the given AAS
		// to the server to which the address was given
		// It also registers the AAS in the registry
		manager.createAAS(aas, AASSERVERPATH);

		// Register the light animator Submodel at the registry
		registry.register(aas.getIdentification(), lightAnimatorSMDescriptor);
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("Shutting down Light Animator...");
				manager.deleteSubmodel(aas.getIdentification(), lightAnimatorSMDescriptor.getIdentifier());
				manager.deleteAAS(aas.getIdentification());
			}
		});

	}

}
