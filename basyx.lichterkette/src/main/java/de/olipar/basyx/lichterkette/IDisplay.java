/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

/**
 * Interface implemented by displays
 */
public interface IDisplay {
    public boolean displayText(String text);
    public boolean cycleSensorValues();
}
