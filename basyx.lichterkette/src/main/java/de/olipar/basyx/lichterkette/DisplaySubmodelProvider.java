/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.util.Arrays;
import java.util.function.Function;

import org.eclipse.basyx.aas.metamodel.map.descriptor.ModelUrn;
import org.eclipse.basyx.submodel.metamodel.api.qualifier.haskind.ModelingKind;
import org.eclipse.basyx.submodel.metamodel.map.Submodel;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.Property;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.dataelement.property.valuetype.ValueType;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.Operation;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.OperationVariable;

/**
 * Display submodel provider. Can create an AAS and Submodel for given
 * display. Can Upload the AAS, host the active Submodel and register it
 * <p>
 * Based in parts on various examples and snippets provided by the Eclipse BaSyx
 * Authors
 *
 * @author peggy
 */
public class DisplaySubmodelProvider extends GeneralSubmodelProvider {
    private final String uniqueID;

    public DisplaySubmodelProvider(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public Submodel createDisplayModel(IDisplay display){
        // Create an empty Submodel
        Submodel displaySubmodel = new Submodel();

        // Set its idShort
        displaySubmodel.setIdShort("display");

        // Set its unique identification
        displaySubmodel.setIdentification(new ModelUrn(uniqueID));

        //Add a function to activate cycling through sensor values
        Function<Object[], Object> cycleFunction = (args) -> display.cycleSensorValues();
        Operation cycleOperation = new Operation(cycleFunction);
        cycleOperation.setIdShort("cycleSensorValues");
        displaySubmodel.addSubmodelElement(cycleOperation);

        //Add a function to display custom text
        Property input = new Property("textToDisplay", "defaultInput");
        input.setModelingKind(ModelingKind.TEMPLATE);
        input.setValueType(ValueType.String);

        Function<Object[], Object> displayTextFunction = (args) -> display.displayText(args[0].toString());
        Operation displayTextOperation = new Operation(displayTextFunction);
        displayTextOperation.setIdShort("displayText");
        OperationVariable var = new OperationVariable(input);
        displayTextOperation.setInputVariables(Arrays.asList(var));
        displaySubmodel.addSubmodelElement(displayTextOperation);

        // Return the Submodel
        return displaySubmodel;
    }

    public void hostUploadAndRegister(IDisplay display, String AASID, String AASShortID, int port){
       // First, a local model is created that is wrapped by a model provider
       Submodel displayModel = createDisplayModel(display);

       super.hostUploadAndRegister(displayModel, AASID, AASShortID, port);
    }
}
