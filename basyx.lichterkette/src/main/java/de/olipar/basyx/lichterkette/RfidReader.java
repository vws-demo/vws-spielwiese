/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.LoggerFactory;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RfidReader implements IRfidReader {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RfidReader.class);

    private final String id;
    private final String scriptLocation;
    private Map<String, Object> lastResult = new HashMap<>();
    public List<Process> processes = new ArrayList<Process>();
    private String displayUrn;
    

    public RfidReader(String id, boolean inDocker){
        this.id = id;
        this.scriptLocation = inDocker ? "/root/app/sensor-scripts/" : "/home/pi/vws-spielwiese/basyx.lichterkette/resources/sensors/";
        this.displayUrn = "urn:de.olipar.basyx:lcd-monitor.local:Lcd_0";
    }

    public RfidReader(String id, boolean inDocker, String displayUrn){
        this.id = id;
        this.scriptLocation = inDocker ? "/root/app/sensor-scripts/" : "/home/pi/vws-spielwiese/basyx.lichterkette/resources/sensors/";
        this.displayUrn = displayUrn;
    }

    @Override
    public Map<String, Object> readTag() {
        Map<String, Object> map = new HashMap<>();
        
        try{
            // stopProcesses();

            // logger.info("Put chip on reader.");
            
            // Process proc = Runtime.getRuntime().exec("python3 " + this.scriptLocation + "read_rfid.py");
            // processes.add(proc);

            logger.info("Trying to read tag");
            publishMqtt("rfidReader/RfidReader_"+id+"/read", " ");

            Map<String, Object> oldResult = lastResult;
            long finish = System.currentTimeMillis() + 60000;
            while(System.currentTimeMillis() < finish){
                if(!lastResult.equals(oldResult)){
                    return lastResult;
                }
                Thread.sleep(1000);
            }
        }
        catch(Exception e){
            logger.info("readTag failed: ", e);
            map.put("error", true);
            map.put("message", e);
            return map;
        }

        publishMqtt("rfidReader/RfidReader_"+id+"/stop", " ");

        map.put("error", true);
        map.put("message", "No chip detected.");
        return map;
    }

    @Override
    public boolean startReadTagLoop() {
        try{
            // stopProcesses();

            // Process proc = Runtime.getRuntime().exec("python3 " + this.scriptLocation + "read_rfid_loop.py");
            // processes.add(proc);
            
            publishMqtt("rfidReader/RfidReader_"+id+"/loop", " ");

            return true;
        }
        catch (Exception e){
            logger.info("Start of read tag loop failed: ", e);
        }
        return false;
    }
    
    @Override
    public boolean writeTag(String text) {
        try{
            //stopProcesses();

            logger.info("Text to write: " + text + " Put the chip on the reader.");
            
            // Process proc = Runtime.getRuntime().exec("python3 " + this.scriptLocation + "write_rfid.py " + text);
            // processes.add(proc);

            publishMqtt("rfidReader/RfidReader_"+id+"/write", text);

            Map<String, Object> oldResult = lastResult;
            long finish = System.currentTimeMillis() + 60000;
            logger.info("lastResult: " + lastResult);
            while(System.currentTimeMillis() < finish){
                if(!lastResult.equals(oldResult)){
                    return true;
                }
                Thread.sleep(1000);
            }
            
            return true;
        }
        catch (Exception e){
            logger.info("Writing failed: ", e);
        }
        publishMqtt("rfidReader/RfidReader_"+id+"/stop", " ");
        return false;
    }

    @Override
    public void subscribeToMqtt(String broker, String topic) {
        MqttClient mqttClient = null;
        try {
			mqttClient = new MqttClient(broker, "RfidController");
		} catch (MqttException e) {
			logger.warn("MQTT client could not be created: " + e.getMessage());
		}
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		connOpts.setAutomaticReconnect(true);
		logger.info("Connecting to MQTT broker");
		try {
            RfidMqttCallback callback = new RfidMqttCallback(this, displayUrn);
            mqttClient.setCallback(callback);
			mqttClient.connect(connOpts);
            mqttClient.subscribe(topic);
            logger.info("Connected");
		} catch (NullPointerException | MqttException e) {
			logger.warn("MQTT client could not connect to broker: " + e.getMessage());
		} 
    }

    private void publishMqtt(String topic, String msg){
        try{
            MqttClient mqttClient = new MqttClient(Common.getMQTTBrokerHost(), "RfidController2");
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setAutomaticReconnect(true);
            mqttClient.connect(connOpts);
            MqttMessage message = new MqttMessage(msg.getBytes());
            message.setQos(0);
            mqttClient.publish(topic, message);
        }
        catch(Exception e){
            logger.info("MQTT connection failed", e);
        }
    }

    public String getID() {
        return id;
    }

    public Map<String, Object> getLastResult(){
        return this.lastResult;
    }

    public void setLastResult(Map<String, Object> lastResult){
        this.lastResult = lastResult;
    }

    public void setDisplayUrn(String urn){
        this.displayUrn = urn;
    }

    private void stopProcesses(){
        if(!processes.isEmpty()){
            for(Process p: processes){
                p.destroy();
            }
        }
    }
}
