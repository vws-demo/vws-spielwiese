/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk & Marcus Rothhaupt
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.util.Map;

/**
 * Interface implemented by controllable sensors
 */
public interface ISensor {

	public Map<String, Object> readAnalogValue();
	public Map<String, Object> readDigitalValue();
}