/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

/**
 * Interface implemented by light controllers
 */
public interface ILightController {

	public void redLight();

	public void greenLight();

	public void blinkLight();

	public void staticLight();

	public void updateLamps();

	public void deactivate();

	public boolean isRedOn();

	public boolean isGreenOn();

	public boolean isActive();

}