/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

import org.slf4j.LoggerFactory;

/**
 * Lightstrand. Creates 10 red and green lights each, uses the
 * hostUploadAndRegister method from LightSwitchSubmodelProvider to create,
 * register and upload the AAS as well as host and register the LightSwitch
 * Submodules
 *
 * @author olipar
 *
 */

public class Lightstrand {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Lightstrand.class);
	private static int redLights;
	private static int greenLights;
	private static int[] redPins;
	private static int[] greenPins;

	private static int getLightAmount(String color) {
		color = color.toUpperCase();
		String uncheckedAmount = System.getenv(color + "_LIGHTS");
		if (uncheckedAmount == null || uncheckedAmount.isEmpty()) {
			logger.warn("No valid amount for " + color.toLowerCase() + " lamps set.");
			logger.warn("You might want to explicitly set a " + color + "_LIGHTS environment variable, eg:");
			logger.warn("$ export " + color + "_LIGHTS=4");
			logger.warn("No " + color.toLowerCase() + " lights will be created.");
			return 0;
		}
		try {
			int uncheckedIntegerAmount = Integer.parseUnsignedInt(uncheckedAmount);
			if (uncheckedIntegerAmount < 0) {
				throw new IllegalArgumentException(color + "_LIGHTS is negative");
			}
			return uncheckedIntegerAmount;
		} catch (IllegalArgumentException e) {
			logger.warn("No valid amount for " + color.toLowerCase() + " lamps set.");
			logger.warn("Make sure " + color.toLowerCase() + " is a positive or zero integer value, eg:");
			logger.warn("$ export " + color + "_LIGHTS=4");
			logger.warn("No " + color.toLowerCase() + " lights will be created.");
			return 0;
		}
	}

	private static int[] getGPIOPins(String color) {
		color = color.toUpperCase();
		String uncheckedPins = System.getenv(color + "_PINS");
		if (uncheckedPins == null || uncheckedPins.isEmpty()) {
			logger.warn("No valid GPIO pins for " + color.toLowerCase() + " lamps set.");
			logger.warn("You might want to explicitly set a " + color + "_PINS environment variable, eg:");
			logger.warn("$ export " + color + "_PINS=[2,3,4,17]");
			logger.warn("State of virtual " + color.toLowerCase() + " lights will not be mirrored  in the real world.");
			return new int[0];
		} else {
			try {
				// Convert the environment variable eg. "[xx,yy,zz]" to int array {xx, yy, zz}
				int[] unfiltered_arr = Arrays.stream(uncheckedPins.substring(1, uncheckedPins.length() - 1).split(","))
						.map(String::trim).mapToInt(Integer::parseInt).toArray();
				// Filter out all non-unique integers
				int[] unique_arr = IntStream.of(unfiltered_arr).distinct().toArray();
				if (unfiltered_arr.length > unique_arr.length) {
					logger.warn("There are non-unique " + color + "_PINS set");
				}
				// Filter out all integers that are not valid GPIO pin numbers
				// ie. not in [0;27]
				IntPredicate isInRange = argument -> argument >= 0 && argument <= 27;
				int[] arr = IntStream.of(unique_arr).filter(isInRange).toArray();
				if (unique_arr.length > arr.length) {
					logger.warn("There are invalid " + color + "_PINS set.");
					logger.warn("Please make sure " + color + "_PIN values are within [0;27]");
				}

				if ((Objects.equals(color, "RED") && arr.length < redLights)
						|| (Objects.equals(color, "GREEN") && arr.length < greenLights)) {
					logger.warn("Not enough valid " + color + "_PINS set.");
					logger.warn("Only the first " + arr.length + " virtual " + color.toLowerCase()
							+ " lights will be mirrored in the real world.");
				}
				return arr;

			} catch (NumberFormatException e) {
				logger.warn("Invalid value in/for" + color + "_PINS set");
				logger.warn("Please make sure the values are integers separated by commas, enclosed in brackets, eg:");
				logger.warn("$ export " + color + "_PINS=[2,3,4,17]");
				logger.warn("State of virtual " + color.toLowerCase() + " lights will not be mirrored on the output.");
				return new int[0];
			}
		}
	}

	private static void setGPIOOutputMode(int[] pins) {
		for (int pin : pins) {
			try {
				Runtime.getRuntime().exec("raspi-gpio set " + pin + " op").waitFor();
			} catch (IOException | InterruptedException e) {
				logger.warn("Could not set GPIO mode OUTPUT on port " + pin + ": " + e.getMessage());
			}
		}
	}

	public static void main(String[] args) {

		redLights = getLightAmount("red");
		greenLights = getLightAmount("green");

		redPins = getGPIOPins("red");
		int[] unfilteredGreenPins = getGPIOPins("green");

		greenPins = Arrays.stream(unfilteredGreenPins).filter(x -> !Arrays.asList(redPins).contains(x)).toArray();
		if (unfilteredGreenPins.length > greenPins.length) {
			logger.warn("There are duplicate pins defined in GREEN_PINS and RED_PINS.");
			logger.warn("The offending values have been filtered from GREEN_PINS.");
		}
		if (greenPins.length < greenLights) {
			logger.warn("Not enough valid GREEN_PINS set.");
			logger.warn(
					"Only the first " + greenPins.length + " virtual green lights will be mirrored in the real world.");
		}

		setGPIOOutputMode(redPins);
		setGPIOOutputMode(greenPins);

		final int HOSTPORT = Common.getHostPort();
		logger.info("Local host port is set to " + HOSTPORT);
		logger.info("Ports " + HOSTPORT + " through including " + (HOSTPORT + redLights + greenLights - 1)
				+ " will be used for lights.");

		for (int i = 0; i < redLights; i++) {
			RedLight light;
			if (i < redPins.length) {
				light = new RedLight(Integer.toString(i), redPins[i]);
			} else {
				light = new RedLight(Integer.toString(i));
			}
			LightSwitchSubmodelProvider lssp = new LightSwitchSubmodelProvider("Lightstrand:redLight_" + light.getID());
			lssp.hostUploadAndRegister(light, "redLight_" + light.getID(), "redLight_" + light.getID(), HOSTPORT + i);
		}
		for (int i = 0; i < greenLights; i++) {
			GreenLight light;
			if (i < greenPins.length) {
				light = new GreenLight(Integer.toString(i), greenPins[i]);
			} else {
				light = new GreenLight(Integer.toString(i));
			}
			LightSwitchSubmodelProvider lssp = new LightSwitchSubmodelProvider(
					"Lightstrand:greenLight_" + light.getID());
			lssp.hostUploadAndRegister(light, "greenLight_" + light.getID(), "greenLight_" + light.getID(),
					HOSTPORT + redLights + i);
		}
	}

}
