/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

/**
 * Interface implemented by light animators
 */
public interface ILightAnimator {

	public void staticLight();

	public void animation1();

	public void animation2();

	public void deactivate();

	public void updateControllers();

	public boolean isActive();
}