/*******************************************************************************
 * Copyright (C) 2022 the Eclipse BaSyx Authors, modified by Oliver Parczyk
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/
package de.olipar.basyx.lichterkette;

import org.eclipse.basyx.components.aas.AASServerComponent;
import org.eclipse.basyx.components.aas.configuration.AASServerBackend;
import org.eclipse.basyx.components.aas.configuration.BaSyxAASServerConfiguration;
import org.eclipse.basyx.components.configuration.BaSyxContextConfiguration;
import org.slf4j.LoggerFactory;

/**
 * This class starts an AAS server and a Registry server
 * 
 * Based in part on various examples and snippets provided by the Eclipse BaSyx
 * Authors
 * 
 * @author schnicke, conradi, olipar
 *
 */
public class AASServer {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AASServer.class);

	public static void main(String[] args) {
		final int HOSTPORT = Common.getHostPort();
		logger.info("Local host port is set to " + HOSTPORT);
		final String REGISTRYPATH = Common.getRegistryPath();
		logger.info("Registry path set to " + REGISTRYPATH);

		startAASServer(REGISTRYPATH, HOSTPORT);
	}

	/**
	 * Startup an empty server at "http://localhost:{HOSTPORT}/"
	 */
	private static void startAASServer(final String REGISTRYPATH, final int HOSTPORT) {
		BaSyxContextConfiguration contextConfig = new BaSyxContextConfiguration(HOSTPORT, "/aasServer");
		BaSyxAASServerConfiguration aasServerConfig = new BaSyxAASServerConfiguration(AASServerBackend.INMEMORY, "",
				REGISTRYPATH);
		AASServerComponent aasServer = new AASServerComponent(contextConfig, aasServerConfig);

		// Start the created server
		aasServer.startComponent();
	}
}
