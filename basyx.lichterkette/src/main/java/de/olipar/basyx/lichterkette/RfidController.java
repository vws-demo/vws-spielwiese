/*******************************************************************************
 * Copyright (C) 2023 Peggy Hölzel
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.slf4j.LoggerFactory;

 /**
  * RfidController. Creates 1 rfidReader, uses the
  * hostUploadAndRegister method from RfidSubmodelProvider to create,
  * register and upload the AAS as well as host and register the Rfid
  * Submodules
  *
  * @author peggy
  */

public class RfidController {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(RfidController.class);
    private static int rfidReaderAmount;

    public static void main(String[] args) {
        final int HOSTPORT = Common.getHostPort();
        
        rfidReaderAmount = getAmount();
        boolean inDocker = Common.inDocker();

        for (int i = 0; i < rfidReaderAmount; i++) {
            RfidReader rfid = new RfidReader(Integer.toString(i), inDocker);
            rfid.subscribeToMqtt(Common.getMQTTBrokerHost(), "rfidReader/RfidReader_" + rfid.getID());
            RfidSubmodelProvider rsp = new RfidSubmodelProvider("RfidController:RfidReader_" + rfid.getID());
            rsp.hostUploadAndRegister(rfid, "RfidReader_" + rfid.getID(), "RfidReader_" + rfid.getID(), HOSTPORT + i);
            rfid.startReadTagLoop();
        }
    }

    private static int getAmount(){
        String amountString = System.getenv("READERAMOUNT");

        if (amountString == null || amountString.isEmpty()) {
            logger.warn("No valid amount for RFID reader set.");
            logger.warn("You might want to explicitly set a READERAMOUNT environment variable.");
            return 0;
        }

        try {
            int amountInt = Integer.parseUnsignedInt(amountString);
            if (amountInt < 0) {
                throw new IllegalArgumentException("READERAMOUNT is negative");
            }
            return amountInt;
        } catch (IllegalArgumentException e) {
            logger.warn("No valid amount for READERAMOUNT sensors set. No RFID readers will be created.");
            return 0;
        }
    }
}
