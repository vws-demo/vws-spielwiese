/*******************************************************************************
 * Copyright (C) 2022 the Eclipse BaSyx Authors, Oliver Parczyk & Marcus Rothhaupt
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import org.eclipse.basyx.aas.manager.ConnectedAssetAdministrationShellManager;
import org.eclipse.basyx.aas.metamodel.api.parts.asset.AssetKind;
import org.eclipse.basyx.aas.metamodel.map.AssetAdministrationShell;
import org.eclipse.basyx.aas.metamodel.map.descriptor.ModelUrn;
import org.eclipse.basyx.aas.metamodel.map.descriptor.SubmodelDescriptor;
import org.eclipse.basyx.aas.metamodel.map.parts.Asset;
import org.eclipse.basyx.aas.registration.proxy.AASRegistryProxy;
import org.eclipse.basyx.submodel.metamodel.map.Submodel;
import org.eclipse.basyx.submodel.metamodel.map.submodelelement.operation.Operation;
import org.eclipse.basyx.submodel.restapi.SubmodelProvider;
import org.eclipse.basyx.vab.modelprovider.api.IModelProvider;
import org.eclipse.basyx.vab.protocol.http.server.BaSyxContext;
import org.eclipse.basyx.vab.protocol.http.server.BaSyxHTTPServer;
import org.eclipse.basyx.vab.protocol.http.server.VABHTTPInterface;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import java.util.function.Function;

/**
 * Sensor switch submodel provider. Can create an AAS and Submodel for given
 * Sensor. Can Upload the AAS, host the active Submodel and register it
 * <p>
 * Based in part on various examples and snippets provided by the Eclipse BaSyx
 * Authors
 *
 * @author olipar, conradi, kuhn, speedstuff
 */
public class SensorSubmodelProvider {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SensorSubmodelProvider.class);
    private final String uniqueID;

    public SensorSubmodelProvider(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public Submodel createMySensorModel(ISensor sensor) {
        // Create an empty Submodel
        Submodel sensorSubmodel = new Submodel();

        // Set its idShort
        sensorSubmodel.setIdShort("sensor");

        // Set its unique identification
        sensorSubmodel.setIdentification(new ModelUrn("Sensor" + uniqueID));

        // Add a function that reads the value from the sensor and implements a functional interface
        Function<Object[], Object> readAnalogValueFunction = (args) -> sensor.readAnalogValue();
        Operation readAnanlogValueOperation = new Operation(readAnalogValueFunction);
        readAnanlogValueOperation.setIdShort("readAnalogValue");
        sensorSubmodel.addSubmodelElement(readAnanlogValueOperation);

        // Add a function that reads the value from the sensor and implements a functional interface
        Function<Object[], Object> readDigitalValueFunction = (args) -> sensor.readDigitalValue();
        Operation readDigitalValueOperation = new Operation(readDigitalValueFunction);
        readDigitalValueOperation.setIdShort("readDigitalValue");
        sensorSubmodel.addSubmodelElement(readDigitalValueOperation);

        // Return the Submodel
        return sensorSubmodel;
    }

    public void hostUploadAndRegister(ISensor sensor, String AASID, String AASShortID, int port) {

        // First, a local model is created that is wrapped by a model provider
        Submodel sensorModel = createMySensorModel(sensor);

        final String HOSTADDRESS = Common.getHostAddress();
        logger.info("Local host address is set to " + HOSTADDRESS);
        logger.info("Local host port is set to " + port);
        final String REGISTRYPATH = Common.getRegistryPath();
        logger.info("Registry path set to " + REGISTRYPATH);
        final String AASSERVERPATH = Common.getAASServerPath();
        logger.info("AAS server path set to " + AASSERVERPATH);


        // Now wrap the model in a SubmodelProvider
        IModelProvider modelProvider = new SubmodelProvider(sensorModel);

        // Now, create the servlet that will provide the http/REST interface for
        // accessing the sensor switch Submodel
        // => Every servlet that is provided by this node is available at
        // http://{HOSTADRESS}:{port}/handson/
        BaSyxContext context = new BaSyxContext("/handson", "", HOSTADDRESS, port);

        // Now, the model provider is attached to a HTTP servlet that enables access to
        // the model
        // in the next steps through a HTTP rest interface
        // => The model will be published using an HTTP-REST interface
        HttpServlet modelServlet = new VABHTTPInterface<IModelProvider>(modelProvider);
        logger.info("Created a servlet for the sensormodel");

        // The provider will be available at
        // http://{HOSTADDRESS}:{port}/handson/sensor/
        // And Submodel can be accessed at:
        // http://{HOSTADDRESS}:{port}/handson/sensor/submodel
        context.addServletMapping("/sensor/*", modelServlet);

        // Start the local HTTP server
        BaSyxHTTPServer server = new BaSyxHTTPServer(context);
        server.start();
        logger.info("HTTP server started");

        // The URN is a unique name, and refers to the asset
        ModelUrn aasURN = new ModelUrn("urn:de.olipar.basyx:" + HOSTADDRESS + ":" + AASID);

        // A digital representation of the asset
        Asset asset = new Asset("sensorAsset", aasURN, AssetKind.INSTANCE);

        // Create the Asset Administration Shell object
        AssetAdministrationShell aas = new AssetAdministrationShell(AASShortID, aasURN, asset);

        // Now add the references of the submodels to the AAS header
        aas.addSubmodel(sensorModel);

        // Explicitly create and add submodel descriptors
        SubmodelDescriptor sensorSMDescriptor = new SubmodelDescriptor(sensorModel,
                "http://" + HOSTADDRESS + ":" + port + "/handson/sensor/submodel");

        // Create a ConnectedAASManager using the registryProxy as its registry
        AASRegistryProxy registry = new AASRegistryProxy(REGISTRYPATH);
        ConnectedAssetAdministrationShellManager manager = new ConnectedAssetAdministrationShellManager(registry);

        // The ConnectedAASManager automatically pushes the given AAS
        // to the server to which the address was given
        // It also registers the AAS in the registry
        manager.createAAS(aas, AASSERVERPATH);

        // Register the sensor switch Submodel at the registry
        registry.register(aas.getIdentification(), sensorSMDescriptor);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                System.out.println("Shutting down Sensor Switch...");
                manager.deleteSubmodel(aas.getIdentification(), sensorSMDescriptor.getIdentifier());
                manager.deleteAAS(aas.getIdentification());
            }
        });
    }

}

