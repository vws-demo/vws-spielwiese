/*******************************************************************************
 * Copyright (C) 2022 Oliver Parczyk & Marcus Rothhaupt
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ******************************************************************************/

package de.olipar.basyx.lichterkette;

import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.HashMap;

public class DigitalTemperatureHumiditySensor implements ISensor {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DigitalTemperatureHumiditySensor.class);

    private final String id;
    private final String scriptLocation;

    public DigitalTemperatureHumiditySensor(String id, boolean inDocker) {
        this.id = id;
        this.scriptLocation = inDocker ? "/root/app/sensor-scripts/" : "/home/pi/vws-spielwiese/basyx.lichterkette/resources/sensors/";
    }

    @Override
    public Map<String, Object> readAnalogValue() {
        Map<String, Object> map = new HashMap<>();
        map.put("error", true);
        map.put("message", "DigitalTempHumidSensor has no analog reading");
        return map;
    }

    @Override
    public Map<String, Object> readDigitalValue() {
        try {
            //curl -X POST http://tempSensor-1.local:4001/handson/sensorSwitch/submodel/submodelElements/readValue/invoke
            Process proc = Runtime.getRuntime().exec("python3 " + this.scriptLocation + "read_digital_temp_humid_sensor.py");

            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));

            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(proc.getErrorStream()));

            // Read the output from the command
            // System.out.println("Here is the standard output of the command:\n");
            String s = null;
            StringBuilder output = new StringBuilder();
            while ((s = stdInput.readLine()) != null) {
                output.append(s);
            }

            // Read any errors from the attempted command
            while ((s = stdError.readLine()) != null) {
                output.append(s);
            }
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(output.toString(), HashMap.class);
            return map;
        } catch (IOException e) {
            logger.warn("Could not read from digitalTempHumidSensor " + e.getMessage());
        }
        Map<String, Object> map = new HashMap<>();
        map.put("error", true);
        map.put("message", "Could not read from digitalTempHumidSensor");
        return map;
    }

    public String getID() {
        return id;
    }
}
