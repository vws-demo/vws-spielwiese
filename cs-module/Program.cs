/*******************************************************************************
 * Copyright (c) 2022 Daniel Kluge
 * Author: Daniel Kluge (daniel.kluge@mailbox.tu-dresden.de)
 * 
 * Original source is available at:
 * https://github.com/eclipse-basyx/basyx-dotnet-examples/blob/65b63180c9959f84d9eeceefd42672f90ccf0a83/HelloAssetAdministrationShell/Program.cs
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Distribution License 1.0 which is available at
 * https://www.eclipse.org/org/documents/edl-v10.html
 *
 * 
 *******************************************************************************/
// using BaSyx.AAS.Client.Http;
using System;
using BaSyx.AAS.Server.Http;
using BaSyx.API.Components;
using BaSyx.Common.UI;
using BaSyx.Common.UI.Swagger;
// using BaSyx.Discovery.mDNS; // Be sure to install the component dependency if you want to use it
using BaSyx.Utils.Settings.Types;
using BaSyx.Registry.Client.Http;
using NLog;
using NLog.Web;
using System.Collections.Generic;

using System.Linq;

namespace GreenLightAdministrationShell
{
    class Program
    {
        // Create logger for the application
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            logger.Info("Starting GreenLightAdministrationShell's HTTP server...");

            // Loading the default server configurations settings from ServerSettings.xml
            // -> This file was part of the example project.
            ServerSettings serverSettings = ServerSettings.LoadSettingsFromFile("ServerSettings.xml");

            // Maybe we want to use another port than default
            // -> Using environment variable PORT
            // else 4000 will be used
            string userPort = Environment.GetEnvironmentVariable("PORT") ?? "4000";
            serverSettings.ServerConfig.Hosting.Urls = new List<string> { "http://+:" + userPort };

            // Initialize generic HTTP-REST interface passing previously loaded server configuration.
            // -> This creates a local webserver which serves UI and the Shell
            AssetAdministrationShellHttpServer server = new AssetAdministrationShellHttpServer(serverSettings);

            // Getting the URL of the registry
            string userRegistry = Environment.GetEnvironmentVariable("REGISTRYPATH") ?? "http://registry.local:4000/registry";

            // Initialize the Registry Client.
            // First we need to create settings because we need to define where the registry is.
            // You can also use the RegistryClientSettings.xml file for this.
            RegistryClientSettings registryClientSettings = RegistryClientSettings.CreateSettings();
            // Set the endpoint URL to what is written in the env var or (if nothing is there) to a default value
            registryClientSettings.RegistryConfig.RegistryUrl = userRegistry;
            RegistryHttpClient registryHttpClient = new RegistryHttpClient(registryClientSettings);

            // We also want to register the component in the AAS Server, same procedure as for the registry
            // But I had some issues and did not find how to even register the component.
            // This is because there is no AssetAdministrationShellRepositoryHttpClient, we can use to upload our AAS.
            // So for the demo it has to be registry only.

            // I will leave this here as it should be something like this but only if the repository client exists in the future.
            // (To use this BaSyx.AAS.Client.Http has to be installed as dependency)
            // string userAAS = Environment.GetEnvironmentVariable("AASSERVERPATH") ?? "http://main.local:4001/aasServer";
            // AssetAdministrationShellRepositoryClientSettings aasClientSettings = AssetAdministrationShellRepositoryClientSettings.CreateSettings();
            // aasClientSettings.AssetAdministrationShellRepository.RepositoryUrl = userAAS;
            // AssetAdministrationShellRepositoryHttpClient aasClient = new AssetAdministrationShellRepositoryHttpClient(aasClientSettings);

            // later RegisterAssetAdministrationShellServer and unregister can be called.

            // Configure the entire application to use your own logger library (here: Nlog)
            server.WebHostBuilder.UseNLog();

            // Instantiate Asset Administration Shell Service
            GreenLightAdministrationShellService shellService = new GreenLightAdministrationShellService();

            // Dictate Asset Administration Shell service to use provided endpoints from the server configuration
            shellService.UseAutoEndpointRegistration(serverSettings.ServerConfig);

            // Assign Asset Administration Shell Service to the generic HTTP-REST interface
            server.SetServiceProvider(shellService);

            // Add Swagger documentation and UI
            // Not needed for our testing and use case but we would have a dead link in the UI without it.
            server.AddSwagger(Interface.AssetAdministrationShell);

            // Add BaSyx Web UI
            server.AddBaSyxUI(PageNames.AssetAdministrationShellServer);

            // Action that gets executued when server is fully started
            server.ApplicationStarted = () =>
            {
                // The next line starts a thread which provides some mDNS functionality and the original comment
                // said that this would also automatically register the shell at the registry.
                // But it never registered anything at the registry even when provided an IP and port.
                // Probably our registry is served on a route other than "/" ("ip:port/registry").
                // As we don't need it in our case, we can disable it.
                // shellService.StartDiscovery();

                // This actually registers our shell at the registry.
                // You could actually use
                // shellService.RegisterAssetAdministrationShell(registryClientSettings);
                // and won't even need to create registryHttpClient, but there is no method
                // shellService.UnregisterAssetAdministrationShell();
                // This means if you want to unregister your shell when stopping the server you still need the client.
                registryHttpClient.CreateOrUpdateAssetAdministrationShellRegistration(shellService.ServiceDescriptor.Identification.Id, shellService.ServiceDescriptor);
            };

            // Action that gets executed when server is shutting down
            server.ApplicationStopping = () =>
            {
                // Unregister the shell from the registry
                registryHttpClient.DeleteAssetAdministrationShellRegistration(shellService.ServiceDescriptor.Identification.Id);

                // Stop mDNS discovery thread
                // shellService.StopDiscovery();
            };

            // Run HTTP server
            server.Run();
        }
    }
}
