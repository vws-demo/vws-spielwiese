/*******************************************************************************
 * Copyright (c) 2022 Daniel Kluge
 * Author: Daniel Kluge (daniel.kluge@mailbox.tu-dresden.de)
 * 
 * Original source is available at:
 * https://github.com/eclipse-basyx/basyx-dotnet-examples/blob/65b63180c9959f84d9eeceefd42672f90ccf0a83/HelloAssetAdministrationShell/HelloAssetAdministrationShellService.cs
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Distribution License 1.0 which is available at
 * https://www.eclipse.org/org/documents/edl-v10.html
 *
 * 
 *******************************************************************************/
using BaSyx.API.Components;
using BaSyx.Models.Core.AssetAdministrationShell;
using BaSyx.Models.Core.AssetAdministrationShell.Generics;
using BaSyx.Models.Core.AssetAdministrationShell.Identification;
using BaSyx.Models.Core.AssetAdministrationShell.Implementations;
using BaSyx.Models.Core.Common;
using BaSyx.Models.Extensions;
using BaSyx.Utils.ResultHandling;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using NLog;

namespace GreenLightAdministrationShell
{
    public class GreenLightAdministrationShellService : AssetAdministrationShellServiceProvider
    {
        private readonly SubmodelServiceProvider lightSubmodelServiceProvider;
        // Creating the logger
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        // Using GPIO-Port
        private string gpioPort = System.Environment.GetEnvironmentVariable("LED_PIN");

        public GreenLightAdministrationShellService()
        {
            if (gpioPort != null)
            {
                try
                {
                    Process exec = Process.Start("raspi-gpio", "set " + gpioPort + " op");
                    exec.WaitForExit();
                    exec = Process.Start("raspi-gpio", "set " + gpioPort + " dl");
                    exec.WaitForExit();
                }
                catch (System.Exception e)
                {
                    logger.Error("Could not set output mode for gpio pin!");
                    logger.Error(e.Message);
                    gpioPort = null;
                }
            }
            // Create a new SubmodelServiceProvider
            lightSubmodelServiceProvider = new SubmodelServiceProvider();
            // Binding the submodel service provider to the lightSwitch submodel
            lightSubmodelServiceProvider.BindTo(AssetAdministrationShell.Submodels["lightSwitch"]);
            // Register methods that are called when the operations defined in the submodule are invoked.
            lightSubmodelServiceProvider.RegisterMethodCalledHandler("activateLight", LightActivateOperationHandler);
            lightSubmodelServiceProvider.RegisterMethodCalledHandler("deactivateLight", LightDeactivateOperationHandler);
            // Register methods that are called when a property is accessed. get- and set-methods (nullable)
            lightSubmodelServiceProvider.RegisterSubmodelElementHandler("lightState", new SubmodelElementHandler(GreenLightElementGetHandler, null));
            // Finally register the submodel service provider as lightSwitch
            this.RegisterSubmodelServiceProvider("lightSwitch", lightSubmodelServiceProvider);
        }

        // Function that is called when "get" is called on the lightState property
        private IValue GreenLightElementGetHandler(ISubmodelElement submodelElement)
        {
            // Get property
            var localProperty = AssetAdministrationShell.Submodels["lightSwitch"].SubmodelElements["lightState"].Cast<IProperty>();
            // Return value and value type
            return new ElementValue(localProperty.Value, localProperty.ValueType);
        }

        // Function that is called when activateLight is invoked
        private Task<OperationResult> LightActivateOperationHandler(IOperation operation, IOperationVariableSet inputArguments, IOperationVariableSet inoutputArguments, IOperationVariableSet outputArguments, CancellationToken cancellationToken)
        {
            // Set the lightState property to "on"
            AssetAdministrationShell.Submodels["lightSwitch"].SubmodelElements["lightState"].Cast<IProperty>().Value = "on";

            // Only try setting the Pin when there is a Pin specified.
            bool result = true;
            if (gpioPort != null)
            {
                // Set the GPIO pin
                try
                {
                    Process exec = Process.Start("raspi-gpio", "set " + gpioPort + " dl");
                    exec.WaitForExit();
                    logger.Info("Green light was turned on!");
                } catch (System.Exception e)
                {
                    logger.Error("Light should turn on, but raspi-gpio cannot be started!");
                    logger.Error(e.Message);
                    result = false;
                }
            } else
            {
                // Print a statement to the console
                logger.Info("Green light was turned on!");
            }
            
            
            // Function will always work so we can return true as result
            return new OperationResult(result);
        }

        // Function that is called when deactivateLight is invoked
        // See above for more detailed description what is done
        private Task<OperationResult> LightDeactivateOperationHandler(IOperation operation, IOperationVariableSet inputArguments, IOperationVariableSet inoutputArguments, IOperationVariableSet outputArguments, CancellationToken cancellationToken)
        {
            AssetAdministrationShell.Submodels["lightSwitch"].SubmodelElements["lightState"].Cast<IProperty>().Value = "off";

            bool result = true;
            if (gpioPort != null)
            {
                try
                {
                    Process exec = Process.Start("raspi-gpio", "set " + gpioPort + " dh");
                    exec.WaitForExit();
                    logger.Info("Green light was turned off!");
                }
                catch (System.Exception e)
                {
                    logger.Error("Light should turn off, but raspi-gpio cannot be started!");
                    logger.Error(e.Message);
                    result = false;
                }
            }
            else
            {
                logger.Info("Green light was turned off!");
            }

            return new OperationResult(result);
        }

        // Function to build the AAS
        public override IAssetAdministrationShell BuildAssetAdministrationShell()
        {
            // Create the AAS with IDs, a description and a asset
            AssetAdministrationShell aas = new AssetAdministrationShell("greenLight_0", new Identifier("urn:de.olipar.basyx:cscomponent:greenLight_0", KeyType.IRI))
            {
                Description = new LangStringSet() { new LangString("en", "A demo dummy light AAS.") },

                Asset = new Asset("lightSwitchAsset", new Identifier("urn:de.olipar.basyx:cscomponent:greenLight_0", KeyType.IRI))
                {
                    Description = new LangStringSet() { new LangString("en", "This is a demo light asset reference from the Asset Administration Shell") },
                    Kind = AssetKind.Instance
                }
            };

            // Create the lightSwitch submodel with an ID
            Submodel lightSwitchSubmodel = new Submodel("lightSwitch", new Identifier("LightSwitchLightstrand:greenLight_0", KeyType.IRI))
            {
                Description = new LangStringSet() { new LangString("enS", "This is a submodel to switch the light.") },
                Kind = ModelingKind.Instance,
                //SemanticId = new Reference(new GlobalKey(KeyElements.Submodel, KeyType.IRI, "LightSwitchLightstrand:greenLight_0"))
            };

            // Initializing the list of submodel elements for the lightSwitch
            lightSwitchSubmodel.SubmodelElements = new ElementContainer<ISubmodelElement>
            {
                // Adding a lightState with the default value of "off" and a description
                new Property<string>("lightState", "on")
                {
                    Description = new LangStringSet() { new LangString("en", "This property shows the state of the light.") }
                },

                // Adding the operation activateLight
                // What function actually will be executed is _not_ defined here but when the submodel service provider is created
                new Operation("activateLight")
                {
                    Description = new LangStringSet() { new LangString("en", "This operation turns the light on.") }
                },

                // Adding the operation deactivateLight
                // What function actually will be executed is _not_ defined here but when the submodel service provider is created
                new Operation("deactivateLight")
                {
                    Description = new LangStringSet() { new LangString("en", "This operation turns the light off.") }
                }
            };

            // Initalizing the list of submodels for our AAS
            // This AAS only contains the lightSwitch submodel we just defined
            aas.Submodels = new ElementContainer<ISubmodel>
            {
                lightSwitchSubmodel
            };

            return aas;
        }
    }
}
