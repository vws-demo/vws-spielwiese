// (c) 2022 Oliver Parczyk
// This code is licensed under MIT license (see LICENSE.txt for details)

#define ID "ProprietaryLight0"
#define RED_LED 32
#define GREEN_LED 33

#define TOPIC_PREFIX "proprietaryProtocol/" ID

#include "EspMQTTClient.h"
#include <ESPmDNS.h>

const char* ssid = "";
const char* password =  "";

char bufIP[20];

EspMQTTClient client;
IPAddress mqttServerIP;
s
void setup()
{
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to Wifi...");
  }

  mqttServerIP = IPAddress(0, 0, 0, 0);
  mdns_init();

  while (mqttServerIP == IPAddress(0, 0, 0, 0)) {
    mqttServerIP = MDNS.queryHost("esp-controller");
    Serial.println("Resolving for esp-controller.local...");
  }

  Serial.print("esp-controller.local found at ");
  Serial.println(mqttServerIP);

  client.enableDebuggingMessages();
  client.setMqttClientName(ID);
  sprintf(bufIP, "%d.%d.%d.%d", mqttServerIP[0], mqttServerIP[1], mqttServerIP[2], mqttServerIP[3]);
  const char* b = bufIP;
  client.setMqttServer(b, "", "", 1883);

  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
}

// This function is called once everything is connected (Wifi and MQTT)
// WARNING : YOU MUST IMPLEMENT IT IF YOU USE EspMQTTClient
void onConnectionEstablished()
{
  client.subscribe(TOPIC_PREFIX "/red/on", [](const String & payload) {
    digitalWrite(RED_LED, LOW);
    Serial.println(ID "-red switched on");
  });

  client.subscribe(TOPIC_PREFIX "/red/off", [](const String & payload) {
    digitalWrite(RED_LED, HIGH);
    Serial.println(ID "-red switched off");
  });

  client.subscribe(TOPIC_PREFIX "/green/on", [](const String & payload) {
    digitalWrite(GREEN_LED, LOW);
    Serial.println(ID "-green switched on");
  });
  
  client.subscribe(TOPIC_PREFIX "/green/off", [](const String & payload) {
    digitalWrite(GREEN_LED, HIGH);
    Serial.println(ID "-red switched off");
  });
}

void loop()
{
  client.loop();
}
