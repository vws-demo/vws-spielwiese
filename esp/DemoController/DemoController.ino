// (c) 2022 Oliver Parczyk & Daniel Kluge
// This code is licensed under MIT license (see LICENSE.txt for details)

#define ANIMATION1_SW 14
bool set_animation_1 = false;
void IRAM_ATTR animation_1_inthandler() { set_animation_1 = true; }
#define ANIMATION2_SW 13
bool set_animation_2 = false;
void IRAM_ATTR animation_2_inthandler() { set_animation_2 = true; }
#define ANIMATOR_STOP_SW 27
bool set_animation_off = false;
void IRAM_ATTR animation_off_inthandler() { set_animation_off = true; }

#define RED_LIGHT_SW 25
bool set_lights_red = false;
void IRAM_ATTR set_red_inthandler() { set_lights_red = true; }
#define GREEN_LIGHT_SW 32
bool set_lights_green = false;
void IRAM_ATTR set_green_inthandler() { set_lights_green = true; }
#define ALL_LIGHTS_SW 26
bool set_all_lights = false;
void IRAM_ATTR set_all_inthandler() { set_all_lights = true; }
#define BLINK_SW 33
bool set_blink = false;
void IRAM_ATTR set_blink_inthandler() { set_blink = true; }
#define CONTROLLER_STOP_SW 35
bool stop_controller = false;
void IRAM_ATTR stop_controller_inthandler() { stop_controller = true; }

#define UPDATE_SW 34
bool update_lights = false;
void IRAM_ATTR update_lights_inthandler() { update_lights = true; }

#define ANIMATOR_RED 16
#define ANIMATOR_GREEN 4
#define CONTROLLER_RED 23
#define CONTROLLER_GREEN 22

#define SERVER_RED 18
#define SERVER_GREEN 17
#define REGISTRY_RED 21
#define REGISTRY_GREEN 19

#include <ESPmDNS.h>
#include <HTTPClient.h>

const char* ssid = "";
const char* password = "";

unsigned long last_mDNS_refresh = 0;

IPAddress AASServerIP = IPAddress(0, 0, 0, 0);
#define AASServerPort 4001
IPAddress RegistryIP = AASServerIP;
#define RegistryPort 4000

HTTPClient http;

void resolve_mdns(String name, IPAddress* target)
{
  do {
    *target = MDNS.queryHost(name);
    Serial.println("Resolving for " + name + ".local...");
  } while (*target == IPAddress(0, 0, 0, 0));

  Serial.print(name + ".local found at ");
  Serial.println(AASServerIP);
}

void setup()
{
  pinMode(ANIMATION1_SW, INPUT);
  attachInterrupt(ANIMATION1_SW, animation_1_inthandler, RISING);
  pinMode(ANIMATION2_SW, INPUT);
  attachInterrupt(ANIMATION2_SW, animation_2_inthandler, RISING);
  pinMode(ANIMATOR_STOP_SW, INPUT);
  attachInterrupt(ANIMATOR_STOP_SW, animation_off_inthandler, RISING);

  pinMode(RED_LIGHT_SW, INPUT);
  attachInterrupt(RED_LIGHT_SW, set_red_inthandler, RISING);
  pinMode(GREEN_LIGHT_SW, INPUT);
  attachInterrupt(GREEN_LIGHT_SW, set_green_inthandler, RISING);
  pinMode(ALL_LIGHTS_SW, INPUT);
  attachInterrupt(ALL_LIGHTS_SW, set_all_inthandler, RISING);
  pinMode(BLINK_SW, INPUT);
  attachInterrupt(BLINK_SW, set_blink_inthandler, RISING);
  pinMode(CONTROLLER_STOP_SW, INPUT);
  attachInterrupt(CONTROLLER_STOP_SW, stop_controller_inthandler, RISING);

  pinMode(UPDATE_SW, INPUT);
  attachInterrupt(UPDATE_SW, update_lights_inthandler, RISING);

  pinMode(ANIMATOR_RED, OUTPUT);
  pinMode(ANIMATOR_GREEN, OUTPUT);
  pinMode(CONTROLLER_RED, OUTPUT);
  pinMode(CONTROLLER_GREEN, OUTPUT);

  pinMode(SERVER_RED, OUTPUT);
  pinMode(SERVER_GREEN, OUTPUT);
  pinMode(REGISTRY_RED, OUTPUT);
  pinMode(REGISTRY_GREEN, OUTPUT);

  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to Wifi...");
  }

  mdns_init();

  resolve_mdns("main", &AASServerIP);
  //resolve_mdns("registry", &RegistryIP);
  RegistryIP = AASServerIP;

  Serial.println("AAS Server: " + AASServerIP.toString() + ":" + AASServerPort);
  Serial.println("Registry: " + RegistryIP.toString() + ":" + RegistryPort);
}

bool isAASServerUp()
{
  bool ret;
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/");
  ret = (http.GET() == 200);
  http.end();

  //Remember: LEDs are active LOW
  digitalWrite(SERVER_GREEN, ret ? LOW : HIGH);
  digitalWrite(SERVER_RED, ret ? HIGH : LOW);
  return ret;
}

bool isRegistryUp()
{
  bool ret;
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //String request = "http://" + RegistryIP.toString() + ":" + RegistryPort + "/registry/api/v1/registry";
  http.begin(RegistryIP.toString(), RegistryPort, "/registry/api/v1/registry");
  ret = (http.GET() == 200);
  http.end();

  //Remember: LEDs are active LOW
  digitalWrite(REGISTRY_GREEN, ret ? LOW : HIGH);
  digitalWrite(REGISTRY_RED, ret ? HIGH : LOW);
  return ret;
}

bool isLightControllerActive()
{
  bool ret;
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the 'isActive' Property of the main Submodel of the LightController
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/isActive/value";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/isActive/value");
  ret = (http.GET() == 200);
  
  if (ret) ret = (http.getString() == "true");

  http.end();
  digitalWrite(CONTROLLER_GREEN, ret ? LOW : HIGH);
  digitalWrite(CONTROLLER_RED, ret ? HIGH : LOW);
  return ret;
}

bool isLightAnimatorActive()
{
  bool ret;
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the 'isActive' Property of the main Submodel of the LightAnimator
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/isActive/value";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/isActive/value");
  ret = (http.GET() == 200);

  if (ret) ret = (http.getString() == "true");
  
  http.end();
  digitalWrite(ANIMATOR_GREEN, ret ? LOW : HIGH);
  digitalWrite(ANIMATOR_RED, ret ? HIGH : LOW);
  return ret;
}

void animation1(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'animation1' of the main Submodel of the LightAnimator
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/animation1/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/animation1/invoke");
  http.POST("");
  http.end();
}

void animation2(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'animation2' of the main Submodel of the LightAnimator
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/animation2/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/animation2/invoke");
  http.POST("");
  http.end();
}

void deactivate_animator(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'deactivateAnimator' of the main Submodel of the LightAnimator
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/deactivateAnimator/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/deactivateAnimator/invoke");
  http.POST("");
  http.end();
}

void activate_red(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'redLight' of the main Submodel of the LightController
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/redLight/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/redLight/invoke");
  http.POST("");
  http.end();
}

void activate_green(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'greenLight' of the main Submodel of the LightController
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/greenLight/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/greenLight/invoke");
  http.POST("");
  http.end();
}

void activate_static(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'staticLight' of the main Submodel of the LightController
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/staticLight/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/staticLight/invoke");
  http.POST("");
  http.end();
}

void activate_blink(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'blinkLight' of the main Submodel of the LightController
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/blinkLight/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/blinkLight/invoke");
  http.POST("");
  http.end();
}

void deactivate_controller(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'deactivateLights' of the main Submodel of the LightController
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/deactivateLights/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/deactivateLights/invoke");
  http.POST("");
  http.end();
}

void update_lists(){
  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331
  //Endpoint for the operation 'updateLamps' of the main Submodel of the LightController
  //String request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/updateLamps/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightControllerAAS/aas/submodels/lightControllerSM/submodel/submodelElements/updateLamps/invoke");
  http.POST("");
  http.end();

  http.setReuse(false); // Somehow there seems to be a bug like this: https://github.com/esp8266/Arduino/issues/8331

  //Endpoint for the operation 'updateControllers' of the main Submodel of the LightAnimator
  //request = "http://" + AASServerIP.toString() + ":" + AASServerPort + "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/updateControllers/invoke";
  http.begin(AASServerIP.toString(), AASServerPort, "/aasServer/shells/urn:de.olipar.basyx:LightAnimatorAAS/aas/submodels/lightAnimatorSM/submodel/submodelElements/updateControllers/invoke");
  http.POST("");
  http.end();
}

void loop()
{
  if (millis() - last_mDNS_refresh > 60000) {
    resolve_mdns("main", &AASServerIP);
    RegistryIP = AASServerIP;
    //resolve_mdns("registry", &RegistryIP);

    last_mDNS_refresh=millis();
  }

  isRegistryUp();
  isAASServerUp();

  isLightControllerActive();
  isLightAnimatorActive();
  
  if(set_animation_1){
    set_animation_1 = false;
    Serial.println("invoke animation1");
    animation1();    
  } if (set_animation_2){
    set_animation_2 = false;
    Serial.println("invoke animation2");
    animation2();
  } if (set_animation_off){
    set_animation_off = false;
    Serial.println("invoke deactivateAnimator");
    deactivate_animator();
  } if (set_lights_red){
    set_lights_red = false;
    Serial.println("invoke redLight");
    activate_red();
  } if (set_lights_green){
    set_lights_green = false;
    Serial.println("invoke greenLight");
    activate_green();
  } if (set_all_lights){
    set_all_lights = false;
    Serial.println("invoke staticLight");
    activate_static();
  } if (set_blink){
    set_blink = false;
    Serial.println("invoke blinkLight");
    activate_blink();
  } if (stop_controller){
    stop_controller = false;
    Serial.println("invoke deactivateLights");
    deactivate_controller();
  } if (update_lights){
    update_lights = false;
    Serial.println("invoke update");
    update_lists();
  } 

  delay(100);
}
